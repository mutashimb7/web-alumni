<?php
session_start();
include 'config/koneksi.php';
$_SESSION['url'] = $url;
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- Basic -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">   

        <!-- Mobile Metas -->
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Site Metas -->
        <title>Alumni - ESQ Business School</title>  
        <meta name="keywords" content="">
        <meta name="description" content="">
        <meta name="author" content="">

        <!-- Site Icons -->
        <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
        <link rel="apple-touch-icon" href="images/apple-touch-icon.png">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <!-- Site CSS -->
        <link rel="stylesheet" href="style.css">
        <!-- ALL VERSION CSS -->
        <link rel="stylesheet" href="css/versions.css">
        <!-- Responsive CSS -->
        <link rel="stylesheet" href="css/responsive.css">
        <!-- Custom CSS -->
        <link rel="stylesheet" href="css/custom.css">

        <!-- Modernizer for Portfolio -->
        <script src="js/modernizer.js"></script>

        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

    </head>
    <body class="host_version">

        <!-- LOADER -->
        <div id="preloader">
            <div class="loader-container">
                <div class="progress-br float shadow">
                    <div class="progress__item"></div>
                </div>
            </div>
        </div>
        <!-- END LOADER -->	

        <!-- Start header -->
        <header class="top-navbar">
            <nav class="navbar navbar-expand-lg navbar-light bg-light fixed-top">
                <div class="container-fluid">
                    <a class="navbar-brand" href="index.php">
                        <img src="images/6.png" alt="" />
                    </a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbars-host" aria-controls="navbars-rs-food" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbars-host">
                        <ul class="navbar-nav ml-auto">
                            <li class="nav-item"><a class="nav-link" href="index.php">Home</a></li>
                            <li class="nav-item active"><a class="nav-link" href="tentang.php">Tentang</a></li>
                            <li class="nav-item"><a class="nav-link" href="forum.php">Forum</a></li>
                            <li class="nav-item"><a class="nav-link" href="blog.php">Blog</a></li>
                            <li class="nav-item"><a class="nav-link" href="dosen.php">Dosen</a></li>
                            <li class="nav-item"><a class="nav-link" href="gallery.php">Gallery</a></li>
                            <li class="nav-item"><a class="nav-link" href="kontak.php">Kontak</a></li>
                        </ul>
                        <ul class="nav navbar-nav navbar-right">
                            <?php if (!isset($_SESSION['id_alumni'])) { ?>
                                <li><a class="hover-btn-new log orange" href="authentication-login.php"><span>Login</span></a></li>
                                <?php } else { ?>
                                <li class="nav-item dropdown">
                                    <?php
                                    $id_alumni = $_SESSION['id_alumni'];
                                    $alumni = mysqli_query($con, "SELECT * FROM user WHERE id_user='$id_alumni'");
                                    $a = mysqli_fetch_array($alumni);
                                    ?>
                                    <a class="hover-btn-new log orange" href="authentication-login.php" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span><?php echo $a['nama']; ?></span></a>
                                    <div class="dropdown-menu dropdown-menu-right user-dd animated">
                                        <a class="dropdown-item" href="profile.php?id_alumni=<?php echo $id_alumni ?>">My Profile</a>
                                        <a class="dropdown-item" href="change_password.php?id_alumni=<?php echo $id_alumni ?>">Change Password</a>
                                        <div class="dropdown-divider"></div>
                                        <a class="dropdown-item" href="pages-chat.php">Message</a>
                                        <div class="dropdown-divider"></div>
                                        <a class="dropdown-item" href="config/logout.php">Logout</a>
                                    </div>
                                </li>
<?php } ?>
                        </ul>
                    </div>
                </div>
            </nav>
        </header>
        <!-- End header -->
        <br><br>
        <div class="all-title-box">
            <div class="container text-center">
                <h1>ALUMNI ESQ BUSINESS SCHOOL<span class="m_1">Integrity,Passion,Kreatifity,Humility,Profesionalisme.</span></h1>
            </div>
        </div>

        <div id="overviews" class="section lb">
            <div class="container">
                <div class="section-title row text-center">
                    <div class="col-md-8 offset-md-2">
                        <h3>Tentang</h3>
                        <p class="lead">ESQ Business School adalah satu-satunya sekolah bisnis di Indonesia berbasis pendidikan karakter yang mendidik para mahasiswanya agar mampu mengelola BISNIS secara profesional dan menguasai TEKNOLOGI informasi sesuai perkembangan zaman.</p>
                    </div>
                </div><!-- end title -->

                <div class="row align-items-center">
                    <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12">
                        <div class="message-box">
                            <h4>Alumni ESQ Business School</h4>
                            <h2>Sejarah Kita</h2>
                            <p>ESQBS didirikan dan dipimpin oleh tokoh pendidikan Prof. Ir. Surna Tjahya Djajadiningrat, M.Sc., Ph.D (hingga 2014) serta tokoh di bidang pendidikan karakter DR.HC. Ary Ginanjar Agustian. ESQBS adalah kampus modern yang merupakan sarana untuk mempersiapkan siswa dalam menghadapi tantangan besar dan menjadi generasi pemimpin berikutnya.</p>

                            <p> Kampus ESQBS menyelenggarakan program pendidikan sarjana (S1), yaitu :</p>
                            <ul class="about-list">
                                <li><i class="fa fa-check-square-o"></i> Program studi Manajemen Bisnis </li>
                                <li><i class="fa fa-check-square-o"></i> Program studi Sistem Informasi Bisnis </li>
                                <li><i class="fa fa-check-square-o"></i> Program studi Ilmu Komputer</li>
                                <li><i class="fa fa-check-square-o"></i> Program studi Second Generation</li>
                            </ul>
                            <a href="#" class="hover-btn-new orange"><span>Learn More</span></a>
                        </div><!-- end messagebox -->
                    </div><!-- end col -->

                    <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12">
                        <div class="post-media wow fadeIn">
                            <img src="images/aboutt.jpg" alt="" class="img-fluid img-rounded">
                        </div><!-- end media -->
                    </div><!-- end col -->
                </div>
                <div class="row align-items-center">
                    <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12">
                        <div class="post-media wow fadeIn">
                            <img src="images/aboutt_1.jpg" alt="" class="img-fluid img-rounded">
                        </div><!-- end media -->
                    </div><!-- end col -->

                    <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12">
                        <div class="message-box">
                            <h3>Beasiswa S2 Computer Science Tokyo Institute of Technology JOB VACANCY  22 Mar 2018 Lowongan Pekerjaan Database Administrators</h3>
                            <p>Yanti Alumni ESQ BS : "Untuk beasiswa, beruntung aku bisa menjadi salah satu penerima beasiswa INPEX Scholarship Foundation yang hanya diberikan kepada tiga orang setiap tahunnya. Aku sudah mengetahui informasi tentang beasiswa ini di beberapa semester terakhir kuliahku dari beberapa grup facebook yang aku ikuti, tapi aku tidak pernah menyangka aku bisa mendapatkan beasiswa ini. Bener-bener tidak menyangka. Tapi jangan berfikir kalau aku adalah orang yang cerdas dengan IPK menjulang tinggi atau jadi mahasiswa berprestasi di Kampus. Ada beberapa hal yang aku yakini menjadi alasan mengapa aku diterima beasiswa ini"</p>

                            <a href="#" class="hover-btn-new orange"><span>Learn More</span></a>
                        </div><!-- end messagebox -->
                    </div><!-- end col -->

                </div><!-- end row -->
            </div><!-- end container -->
        </div><!-- end section -->

        <div class="hmv-box">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4 col-md-6 col-12">
                        <div class="inner-hmv">
                            <div class="icon-box-hmv"><i class="flaticon-achievement"></i></div>
                            <h3>Misi</h3>
                            <div class="tr-pa">M</div>
                            <p>"Menjadi pusat ilmu pengetahuan, teknologi, dan kebudayaan yang unggul dan berdaya saing, melalui upaya mencerdasakan kehidupan bangsa untuk meningkatkan kesejahteraan masyarakat, sehingga berkontribusi bagi pembangunan masyarakat Indonesia dan dunia".</p>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-12">
                        <div class="inner-hmv">
                            <div class="icon-box-hmv"><i class="flaticon-eye"></i></div>
                            <h3>Misi</h3>
                            <div class="tr-pa">V</div>
                            <ul class="about-list">
                                <li><i class="fa fa-check-square-o"></i> Menyediakan akses yang luas dan adil, serta pendidikan dan pengajaran yang berkualitas. </li>
                                <li><i class="fa fa-check-square-o"></i> Menyelenggarakan kegiatan Tridharma yang bermutu dan relevan dengan tantangan nasional serta global. </li>
                                <li><i class="fa fa-check-square-o"></i> Menciptakan lulusan yang berintelektualitas tinggi, berbudi luhur dan mampu bersaing secara global.</li>
                                <li><i class="fa fa-check-square-o"></i> Menciptakan iklim akademik yang mampu mendukung perwujudan visi ESQ BS</li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-12">
                        <div class="inner-hmv">
                            <div class="icon-box-hmv"><i class="flaticon-history"></i></div>
                            <h3>History</h3>
                            <div class="tr-pa">H</div>
                            <p>Menghadapi tantangan global yang kian kompetitif diperlukan sumber daya manusia handal yang tidak hanya memiliki kecerdasan intelektual, namun juga kecerdasan emosi (EQ) dan kecerdasan spiritual (SQ). Akan tetapi, pendidikan tinggi saat ini umumnya hanya menawarkan pengembangan kecerdasan intelektual (IQ).</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div id="testimonials" class="parallax section db parallax-off" style="background-image:url('images/slider-03.jpg');">
            <div class="container">
                <div class="section-title text-center">
                    <h3>Testimoni Alumni dan Mahasiswa</h3>
                    <p>Integrity,Passion,Kreatifity,Humility,Profesionalisme. </p>
                </div>
                <!-- end title -->
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <div class="testi-carousel owl-carousel owl-theme">
                            <div class="testimonial clearfix">
                                <div class="testi-meta">
                                    <img src="images/testi_04.jpg" alt="" class="img-fluid">
                                    <h4>Andi Ahmad Akhsan</h4>
                                </div>
                                <div class="desc">
                                    <h3><i class="fa fa-quote-left"></i> Juara 1 UNIVATION 2018</h3>
                                    <p class="lead">Di ESQ Business School,kemampuan berorganisasi dan berbisnis saya semakin diasah,difasilitasin banget sama kampuS,di ESQ Businnes School gak ada senioritas dan saya jauh lebih produktif. </p>
                                </div>
                                <!-- end testi-meta -->
                            </div>
                            <!-- end testimonial -->
                            <div class="testimonial clearfix">
                                <div class="testi-meta">
                                    <img src="images/testi_05.jpg" alt="" class="img-fluid">
                                    <h4>Ahmad Reza Hariyadi</h4>
                                </div>
                                <div class="desc">
                                    <h3><i class="fa fa-quote-left"></i> juara 1 UNIVATION 2016</h3>
                                    <p class="lead">Mantap! Metode pembelajaran di ESQ Business School tidak hanya beriorientasi pada akademik,namun juga mengajarkan kita pada arti kehidupan,serta menjadi penggerak terwujudnya Indonesia Emas. </p>
                                </div>
                                <!-- end testi-meta -->
                            </div>
                            <!-- end testimonial -->
                            <div class="testimonial clearfix">
                                <div class="testi-meta">
                                    <img src="images/testi_06.jpg" alt="" class="img-fluid ">
                                    <h4>Rahmi Amalia </h4>
                                </div>
                                <div class="desc">
                                    <h3><i class="fa fa-quote-left"></i> Alumni terbaik dari Aceh</h3>
                                    <p class="lead">Belajar di ESQ Business School ini memang the best.
                                        kita bukan berbicara berapa banyak keuntungan yangkita dapat,tapi kita berbicara tentang berapa banyak orang yang sudah kita bantu. </p>
                                </div>
                                <!-- end testi-meta -->
                            </div>

                            <!-- end testimonial -->
                        </div>
                        <!-- end carousel -->
                    </div>
                    <!-- end col -->
                </div>
                <!-- end row -->
            </div>
            <!-- end container -->
        </div>

        <footer class="footer">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4 col-md-4 col-xs-12">
                        <div class="widget clearfix">
                            <div class="widget-title">
                                <h3>Tentang</h3>
                            </div>
                            <p>Spritual,Kreativitas dan Intelektual menjadi kekuatan ESQ Business School dalam pembentukan karakter mahasiswanya.</p>
                            <div class="footer-right">
                                <ul class="footer-links-soi">
                                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                    <li><a href="#"><i class="fa fa-github"></i></a></li>
                                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                    <li><a href="#"><i class="fa fa-dribbble"></i></a></li>
                                    <li><a href="#"><i class="fa fa-pinterest"></i></a></li>
                                </ul><!-- end links -->
                            </div>
                        </div><!-- end clearfix -->
                    </div><!-- end col -->

                    <div class="col-lg-4 col-md-4 col-xs-12">
                        <div class="widget clearfix">
                            <div class="widget-title">
                                <h3>Link Informati</h3>
                            </div>
                            <ul class="footer-links">

                                <li><a href="index.php">Home</a></li>
                                <li><a href="tentang.php">About</a></li>
                                <li><a href="forum.php">Forum</a></li>
                                <li><a href="blog.php">Blog</a></li>
                                <li><a href="dosen.php">Dosen</a></li>
                                <li><a href="gallery.php">Gallery</a></li>
                                <li><a href="kontak.php">Contact</a></li>
                            </ul><!-- end links -->
                        </div><!-- end clearfix -->
                    </div><!-- end col -->

                    <div class="col-lg-4 col-md-4 col-xs-12">
                        <div class="widget clearfix">
                            <div class="widget-title">
                                <h3>Kontak</h3>
                            </div>

                            <ul class="footer-links">
                                <li><a href="mailto:#">alumni165@esqbs.com</a></li>
                                <li><a href="#">www.alumniesqbs.com</a></li>
                                <li>40 jln.TB.Simatupang,Cilandak Timur,Jakarta Selatan,Jakarta.</li>
                                <li>+61 3 8376 6284</li>
                            </ul><!-- end links -->
                        </div><!-- end clearfix -->
                    </div><!-- end col -->

                </div><!-- end row -->
            </div><!-- end container -->
        </footer><!-- end footer -->

        <div class="copyrights">
            <div class="container">
                <div class="footer-distributed">
                    <div class="footer-center">                   
                        <p class="footer-company-name">All Rights Reserved. &copy; 2018 <a href="#">SmartEDU</a> Design By : <a href="https://html.design/">html design</a></p>
                    </div>
                </div>
            </div><!-- end container -->
        </div><!-- end copyrights -->

        <a href="#" id="scroll-to-top" class="dmtop global-radius"><i class="fa fa-angle-up"></i></a>

        <!-- ALL JS FILES -->
        <script src="js/all.js"></script>
        <!-- ALL PLUGINS -->
        <script src="js/custom.js"></script>

    </body>
</html>
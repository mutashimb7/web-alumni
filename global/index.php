<?php
session_start();
include 'config/koneksi.php';
$_SESSION['url'] = $url;
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- Basic -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Mobile Metas -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Site Metas -->
        <title>Alumni - ESQ Business School</title>
        <meta name="keywords" content="">
        <meta name="description" content="">
        <meta name="author" content="">
        <!-- Site Icons -->
        <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
        <link rel="apple-touch-icon" href="images/apple-touch-icon.png">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <!-- Site CSS -->
        <link rel="stylesheet" href="style.css">
        <!-- ALL VERSION CSS -->
        <link rel="stylesheet" href="css/versions.css">
        <!-- Responsive CSS -->
        <link rel="stylesheet" href="css/responsive.css">
        <!-- Custom CSS -->
        <link rel="stylesheet" href="css/custom.css">
        <!-- Modernizer for Portfolio -->
        <script src="js/modernizer.js"></script>
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>

    <body class="host_version">
        <!-- LOADER -->
        <div id="preloader">
            <div class="loader-container">
                <div class="progress-br float shadow">
                    <div class="progress__item"></div>
                </div>
            </div>
        </div>
        <!-- END LOADER -->
        <!-- Start header -->
        <header class="top-navbar">
            <nav class="navbar navbar-expand-lg navbar-light bg-light fixed-top">
                <div class="container-fluid">
                    <a class="navbar-brand" href="index.php">
                        <img src="images/6.png" alt="" />
                    </a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbars-host" aria-controls="navbars-rs-food" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbars-host">
                        <ul class="navbar-nav ml-auto">
                            <li class="nav-item active"><a class="nav-link" href="index.php">Home</a></li>
                            <li class="nav-item"><a class="nav-link" href="tentang.php">Tentang</a></li>
                            <li class="nav-item"><a class="nav-link" href="forum.php">Forum</a></li>
                            <li class="nav-item"><a class="nav-link" href="blog.php">Blog</a></li>
                            <li class="nav-item"><a class="nav-link" href="dosen.php">Dosen</a></li>
                            <li class="nav-item"><a class="nav-link" href="gallery.php">Gallery</a></li>
                            <li class="nav-item"><a class="nav-link" href="kontak.php">Kontak</a></li>
                        </ul>
                        <ul class="nav navbar-nav navbar-right">
                            <?php if (!isset($_SESSION['id_alumni'])) { ?>
                                <li><a class="hover-btn-new log orange" href="authentication-login.php"><span>Login</span></a></li>
                            <?php } else { ?>
                                <li class="nav-item dropdown">
                                    <?php
                                    $id_alumni = $_SESSION['id_alumni'];
                                    $alumni = mysqli_query($con, "SELECT * FROM user WHERE id_user='$id_alumni'");
                                    $a = mysqli_fetch_array($alumni);
                                    ?>
                                    <a class="hover-btn-new log orange" href="authentication-login.php" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span><?php echo $a['nama']; ?></span></a>
                                    <div class="dropdown-menu dropdown-menu-right user-dd animated">
                                        <a class="dropdown-item" href="profile.php?id_alumni=<?php echo $id_alumni ?>">My Profile</a>
                                        <a class="dropdown-item" href="change_password.php?id_alumni=<?php echo $id_alumni ?>">Change Password</a>
                                        <div class="dropdown-divider"></div>
                                        <a class="dropdown-item" href="pages-chat.php">Message</a>
                                        <div class="dropdown-divider"></div>
                                        <a class="dropdown-item" href="config/logout.php">Logout</a>
                                    </div>
                                </li>
                            <?php } ?>
                        </ul>
                    </div>
                </div>
            </nav>
        </header>
        <!-- End header -->
        <br><br>
        <div id="carouselExampleControls" class="carousel slide bs-slider box-slider" data-ride="carousel" data-pause="hover" data-interval="false">
            <!-- Indicators -->
            <ol class="carousel-indicators">
                <li data-target="#carouselExampleControls" data-slide-to="0" class="active"></li>
                <li data-target="#carouselExampleControls" data-slide-to="1"></li>
                <li data-target="#carouselExampleControls" data-slide-to="2"></li>
            </ol>
            <div class="carousel-inner" role="listbox">
                <div class="carousel-item active">
                    <div id="home" class="first-section" style="background-image:url('images/2.jpg');">
                        <div class="dtab">
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-12 col-sm-12 text-center">
                                        <div class="big-tagline">
                                            <h2><strong>ALUMNI </strong> ESQ BUSINESS SCHOOL</h2>
                                            <p class="lead">Tidak ada kesuksesan yang bisa dicapai seperti membalikkan telapak tangan.
                                                Tidak ada keberhasilan tanpa kerja keras, keuletan, kegigihan, dan kedisiplinan.</p>
                                            <a href="#" class="hover-btn-new"><span>Contact Us</span></a> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <a href="#" class="hover-btn-new"><span>Read More</span></a>
                                        </div>
                                    </div>
                                </div>
                                <!-- end row -->
                            </div>
                            <!-- end container -->
                        </div>
                    </div>
                    <!-- end section -->
                </div>
                <div class="carousel-item">
                    <div id="home" class="first-section" style="background-image:url('images/1.jpg');">
                        <div class="dtab">
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-12 col-sm-12 text-center">
                                        <div class="big-tagline">
                                            <h2 data-animation="animated zoomInRight"><strong>ALUMNI</strong> ESQ BUSINESS SCHOOL</h2>
                                            <p class="lead" data-animation="animated fadeInLeft">Kekuatan tidak berasal dari kemenanganmu, perjuanganmu lah yang mengembangkan kekuatanmu.
                                                Ketika kamu melewati waktu-waktu sulit dan memilih untuk tidak menyerah,
                                                itulah arti dari kekuatan.</p>
                                            <a href="#" class="hover-btn-new"><span>Contact Us</span></a> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <a href="#" class="hover-btn-new"><span>Read More</span></a>
                                        </div>
                                    </div>
                                </div>
                                <!-- end row -->
                            </div>
                            <!-- end container -->
                        </div>
                    </div>
                    <!-- end section -->
                </div>
                <div class="carousel-item">
                    <div id="home" class="first-section" style="background-image:url('images/slider-03.jpg');">
                        <div class="dtab">
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-12 col-sm-12 text-center">
                                        <div class="big-tagline">
                                            <h2 data-animation="animated zoomInRight"><strong>ALUMNI</strong> ESQ BUSINESS SCHOOL</h2>
                                            <p class="lead" data-animation="animated fadeInLeft"><strong>1</strong> HATI <strong>6</strong> PRINSIP <strong>5</strong> LANGKAH</p>
                                            <a href="#" class="hover-btn-new"><span>Contact Us</span></a> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <a href="#" class="hover-btn-new"><span>Read More</span></a>
                                        </div>
                                    </div>
                                </div>
                                <!-- end row -->
                            </div>
                            <!-- end container -->
                        </div>
                    </div>
                    <!-- end section -->
                </div>
                <!-- Left Control -->
                <a class="new-effect carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                    <span class="fa fa-angle-left" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <!-- Right Control -->
                <a class="new-effect carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                    <span class="fa fa-angle-right" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
        <div id="overviews" class="section wb">
            <div class="container">
                <div class="section-title row text-center">
                    <div class="col-md-8 offset-md-2">
                        <h3>Tentang</h3>
                        <p class="lead">ESQ Business School adalah satu-satunya sekolah bisnis di Indonesia berbasis pendidikan karakter yang mendidik para mahasiswanya agar mampu mengelola BISNIS secara profesional dan menguasai TEKNOLOGI informasi sesuai perkembangan zaman.</p>
                    </div>
                </div>
                <!-- end title -->
                <div class="row align-items-center">
                    <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12">
                        <div class="message-box">
                            <h4> (LKTIN) 2019</h4>
                            <h2>Kerjasama ESQ Dengan Perusahaan Tambang Emas</h2>
                            <p>ESQ Business School mempunyai beberapa kerjasama dengan Perusahaan-Perusahaan yang ternama. </p>
                            <p> Perusahaan Tambang Emas Indonesia memulai kerjasama dengan Kampus ESQ Business School untuk mengirimkan beberapa mahasiswa unggulan untuk melakukan project penelitian tentang Emas yang tertimbun dibawah bagunan Menara 165.
                                Kerja sama ini berlaku selama 1 tahun setiap tahunnya. </p>
                            <a href="#" class="hover-btn-new orange"><span>Learn More</span></a>
                        </div>
                        <!-- end messagebox -->
                    </div>
                    <!-- end col -->
                    <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12">
                        <div class="post-media wow fadeIn">
                            <img src="images/44.jpg" alt="" class="img-fluid img-rounded">
                        </div>
                        <!-- end media -->
                    </div>
                    <!-- end col -->
                </div>
                <div class="row align-items-center">
                    <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12">
                        <div class="post-media wow fadeIn">
                            <img src="images/55.jpg" alt="" class="img-fluid img-rounded">
                        </div>
                        <!-- end media -->
                    </div>
                    <!-- end col -->
                    <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12">
                        <div class="message-box">
                            <h2>ESQ Business School Menjadi Kampus Bisinis Terbaik</h2>
                            <p>ESQ Business School adalah satu-satunya sekolah bisnis di Indonesia berbasis pendidikan karakter yang mendidik para mahasiswanya agar mampu mengelola BISNIS secara profesional dan menguasai TEKNOLOGI informasi sesuai perkembangan
                                zaman.</p>
                            <p>Wisuda alumni ESQ Business Scool Tahun 2018/2019 berlangsung pada tanggal 30 Juni 2019. jurusan Business Management meluluskan 13 mahasiswa, Information System meluluskan 8 mahasiwa dan Computer Science 15 mahasiswa.</p>
                            <a href="#" class="hover-btn-new orange"><span>Learn More</span></a>
                        </div>
                        <!-- end messagebox -->
                    </div>
                    <!-- end col -->
                </div>
                <!-- end row -->
            </div>
            <!-- end container -->
        </div>
        <!-- end section -->
        <section class="section lb page-section">
            <div class="container">
                <div class="section-title row text-center">
                    <div class="col-md-8 offset-md-2">
                        <h3>Gallery Alumni ESQ Business School 2018/2019</h3>
                        <p class="lead">Manfaatkanlah masa mudamu sebaik-baiknya. Karena harapan dunia selalu bertumpu pada pemuda.</p>
                    </div>
                </div>
                <!-- end title -->
                <div class="timeline">
                    <div class="timeline__wrap">
                        <div class="timeline__items">
                            <?php
                            $gallery = mysqli_query($con, "SELECT * FROM gallery ORDER BY id_gallery DESC LIMIT 8");
                            while ($g = mysqli_fetch_array($gallery)) {?>
                            <div class="timeline__item">
                                <div class="timeline__content img-bg-01">
                                    <div class="post-media wow fadeIn">
                                        <img src="<?php echo '../img/gallery/' . $g['photo']; ?>" alt="" class="img-fluid img-rounded">
                                    </div>
                                </div>
                            </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <div class="section cl">
            <div class="container">
                <div class="row text-left stat-wrap">
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <?php
                        $x = mysqli_query($con, "SELECT * FROM user WHERE level='alumni'");
                        $qty = 0;
                        while ($a = mysqli_fetch_array($x)) {
                            $qty++;
                        }
                        ?>
                        <span data-scroll class="global-radius icon_wrap effect-1 alignleft"><i class="fas fa-graduation-cap"></i></span>
                        <p class="stat_count"><?php echo $qty; ?></p>
                        <h3>Alumni</h3>
                    </div>
                    <!-- end col -->
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <?php
                        $x = mysqli_query($con, "SELECT * FROM dosen");
                        $qty = 0;
                        while ($a = mysqli_fetch_array($x)) {
                            $qty++;
                        }
                        ?>
                        <span data-scroll class="global-radius icon_wrap effect-1 alignleft"><i class="fas fa-user-md"></i></span>
                        <p class="stat_count"><?php echo $qty; ?></p>
                        <h3>Dosen</h3>
                    </div>
                    <!-- end col -->
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <?php
                        $x = mysqli_query($con, "SELECT * FROM blog");
                        $qty = 0;
                        while ($a = mysqli_fetch_array($x)) {
                            $qty++;
                        }
                        ?>
                        <span data-scroll class="global-radius icon_wrap effect-1 alignleft"><i class="fas fa-newspaper"></i></span>
                        <p class="stat_count"><?php echo $qty; ?></p>
                        <h3>Blog</h3>
                    </div>
                    <!-- end col -->
                </div>
                <!-- end row -->
            </div>
            <!-- end container -->
        </div>
        <!-- end section -->
        <div id="testimonials" class="parallax section db parallax-off" style="background-image:url('images/slider-03.jpg');">
            <div class="container">
                <div class="section-title text-center">
                    <h3>Testimoni Alumni dan Mahasiswa</h3>
                    <p>Integrity,Passion,Kreatifity,Humility,Profesionalisme. </p>
                </div>
                <!-- end title -->
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <div class="testi-carousel owl-carousel owl-theme">
                            <div class="testimonial clearfix">
                                <div class="testi-meta">
                                    <img src="images/testi_04.jpg" alt="" class="img-fluid">
                                    <h4>Andi Ahmad Akhsan</h4>
                                </div>
                                <div class="desc">
                                    <h3><i class="fa fa-quote-left"></i> Juara 1 UNIVATION 2018</h3>
                                    <p class="lead">Di ESQ Business School,kemampuan berorganisasi dan berbisnis saya semakin diasah,difasilitasin banget sama kampuS,di ESQ Businnes School gak ada senioritas dan saya jauh lebih produktif. </p>
                                </div>
                                <!-- end testi-meta -->
                            </div>
                            <!-- end testimonial -->
                            <div class="testimonial clearfix">
                                <div class="testi-meta">
                                    <img src="images/testi_05.jpg" alt="" class="img-fluid">
                                    <h4>Ahmad Reza Hariyadi</h4>
                                </div>
                                <div class="desc">
                                    <h3><i class="fa fa-quote-left"></i> juara 1 UNIVATION 2016</h3>
                                    <p class="lead">Mantap! Metode pembelajaran di ESQ Business School tidak hanya beriorientasi pada akademik,namun juga mengajarkan kita pada arti kehidupan,serta menjadi penggerak terwujudnya Indonesia Emas. </p>
                                </div>
                                <!-- end testi-meta -->
                            </div>
                            <!-- end testimonial -->
                            <div class="testimonial clearfix">
                                <div class="testi-meta">
                                    <img src="images/testi_06.jpg" alt="" class="img-fluid ">
                                    <h4>Rahmi Amalia </h4>
                                </div>
                                <div class="desc">
                                    <h3><i class="fa fa-quote-left"></i> Alumni terbaik dari Aceh</h3>
                                    <p class="lead">Belajar di ESQ Business School ini memang the best.
                                        kita bukan berbicara berapa banyak keuntungan yangkita dapat,tapi kita berbicara tentang berapa banyak orang yang sudah kita bantu. </p>
                                </div>
                                <!-- end testi-meta -->
                            </div>

                            <!-- end testimonial -->
                        </div>
                        <!-- end carousel -->
                    </div>
                    <!-- end col -->
                </div>
                <!-- end row -->
            </div>
            <!-- end container -->
        </div>
        <!-- end section -->
        <footer class="footer">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4 col-md-4 col-xs-12">
                        <div class="widget clearfix">
                            <div class="widget-title">
                                <h3>Tentang</h3>
                            </div>
                            <p> Spritual,Kreativitas dan Intelektual menjadi kekuatan ESQ Business School dalam pembentukan karakter mahasiswanya.</p>
                            <div class="footer-right">
                                <ul class="footer-links-soi">
                                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                    <li><a href="#"><i class="fa fa-github"></i></a></li>
                                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                    <li><a href="#"><i class="fa fa-dribbble"></i></a></li>
                                    <li><a href="#"><i class="fa fa-pinterest"></i></a></li>
                                </ul>
                                <!-- end links -->
                            </div>
                        </div>
                        <!-- end clearfix -->
                    </div>
                    <!-- end col -->
                    <div class="col-lg-4 col-md-4 col-xs-12">
                        <div class="widget clearfix">
                            <div class="widget-title">
                                <h3>Link Informasi </h3>
                            </div>
                            <ul class="footer-links">
                                <li><a href="index.php">Home</a></li>
                                <li><a href="tentang.php">About</a></li>
                                <li><a href="forum.php">Forum</a></li>
                                <li><a href="blog.php">Blog</a></li>
                                <li><a href="dosen.php">Dosen</a></li>
                                <li><a href="gallery.php">Gallery</a></li>
                                <li><a href="kontak.php">Contact</a></li>
                            </ul>
                            <!-- end links -->
                        </div>
                        <!-- end clearfix -->
                    </div>
                    <!-- end col -->
                    <div class="col-lg-4 col-md-4 col-xs-12">
                        <div class="widget clearfix">
                            <div class="widget-title">
                                <h3>Detail Kontak</h3>
                            </div>
                            <ul class="footer-links">
                                <li><a href="mailto:#">alumni165@esqbs.com</a></li>
                                <li><a href="#">www.alumniesqbs.com</a></li>
                                <li>40 jln.TB.Simatupang,Cilandak Timur,Jakarta Selatan,Jakarta.</li>
                                <li>+61 3 8376 6284</li>
                            </ul>
                            <!-- end links -->
                        </div>
                        <!-- end clearfix -->
                    </div>
                    <!-- end col -->
                </div>
                <!-- end row -->
            </div>
            <!-- end container -->
        </footer>
        <!-- end footer -->
        <div class="copyrights">
            <div class="container">
                <div class="footer-distributed">
                    <div class="footer-center">
                        <p class="footer-company-name">All Rights Reserved. &copy; 2018 <a href="#">SmartEDU</a> Design By : <a href="https://html.design/">html design</a></p>
                    </div>
                </div>
            </div>
            <!-- end container -->
        </div>
        <!-- end copyrights -->
        <!-- Modal -->
        <div class="modal fade" id="failLogin" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Username atau Password salah!</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        <a href="#" id="scroll-to-top" class="dmtop global-radius"><i class="fa fa-angle-up"></i></a>
        <!-- ALL JS FILES -->
        <script src="js/all.js"></script>
        <!-- ALL PLUGINS -->
        <script src="js/custom.js"></script>
        <script src="js/timeline.min.js"></script>
        <script>
            timeline(document.querySelectorAll('.timeline'), {
                forceVerticalMode: 700,
                mode: 'horizontal',
                verticalStartPosition: 'left',
                visibleItems: 4
            });
        </script>
    </body>

</html>
<?php

// definisikan koneksi ke database
$server = "localhost";
$username = "root";
$password = "";
$database = "alumnidb";

$protocol = ((!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
$url = $protocol . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];

// Koneksi dan memilih database di server
$con = mysqli_connect($server, $username, $password) or die("Koneksi gagal");
mysqli_select_db($con, $database) or die("Database tidak bisa dibuka");
?>

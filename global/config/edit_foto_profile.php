<?php

include 'koneksi.php';
$id_alumni = $_GET['id_alumni'];

$ekstensi_diperbolehkan = array('png', 'jpg');
$fileName = $_FILES['photo_profile']['name'];
$x = explode('.', $fileName);
$ekstensi = strtolower(end($x));
$ukuran = $_FILES['photo_profile']['size'];
$file_tmp = $_FILES['photo_profile']['tmp_name'];
$lokasi = "../../img/photo_profile/";
$nama_baru = $id_alumni . '.' . end($x);

if (in_array($ekstensi, $ekstensi_diperbolehkan) === true) {
    if ($ukuran < 10044070) {
        $p = mysqli_fetch_array(mysqli_query($con, "SELECT * FROM user WHERE id_user='$id_alumni'"));
        if ($p['photo_profile'] != 'default.jpg') {
            $target = "../../img/photo_profile/" . $p['photo_profile'];
            unlink($target);
        }

        move_uploaded_file($file_tmp, $lokasi . $nama_baru);
        mysqli_query($con, "UPDATE user SET photo_profile='$nama_baru' WHERE id_user = '$id_alumni' ");
    }
}
header("location:../profile.php?id_alumni=$id_alumni");
?>
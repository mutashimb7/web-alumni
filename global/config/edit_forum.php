<?php

include "koneksi.php";
$id_forum = $_GET['id_forum'];
$nama = addslashes($_POST['nama']);
$id_kategori_forum = $_POST['id_kategori_forum'];
$fileName = $_FILES['photo']['name'];

if ($fileName == null) {
    mysqli_query($con, "UPDATE forum SET nama='$nama', id_kategori_forum='$id_kategori_forum' WHERE id_forum='$id_forum'");
} else {
    $ekstensi_diperbolehkan = array('png', 'jpg');
    $x = explode('.', $fileName);
    $ekstensi = strtolower(end($x));
    $ukuran = $_FILES['photo']['size'];
    $file_tmp = $_FILES['photo']['tmp_name'];
    $lokasi = "../../img/forum/";
    $nama_baru = $id_forum . '.' . end($x);

    if (in_array($ekstensi, $ekstensi_diperbolehkan) === true) {
        if ($ukuran < 10044070) {
            $f = mysqli_fetch_array(mysqli_query($con, "SELECT * FROM forum WHERE id_forum='$id_forum'"));
            $target = "../../img/forum/" . $f['photo'];
            unlink($target);

            move_uploaded_file($file_tmp, $lokasi . $nama_baru);
            mysqli_query($con, "UPDATE forum SET nama='$nama', photo='$nama_baru', id_kategori_forum='$id_kategori_forum' WHERE id_forum='$id_forum'");
        }
    }
}

header("location:../forum-single.php?id_forum=$id_forum");

<?php
session_start();
include 'config/koneksi.php';
if (!isset($_SESSION['id_alumni'])) {
    header("location:authentication-login.php");
}
if (!isset($_GET['id_forum']) || $_GET['id_forum'] == null) {
    header('location:forum.php');
} else {
    $id_forum = $_GET['id_forum'];
    $id_alumni = $_SESSION['id_alumni'];
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- Basic -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <!-- Mobile Metas -->
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Site Metas -->
        <title>Alumni - ESQ Business School</title>
        <meta name="keywords" content="">
        <meta name="description" content="">
        <meta name="author" content="">

        <!-- Site Icons -->
        <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
        <link rel="apple-touch-icon" href="images/apple-touch-icon.png">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <!-- Site CSS -->
        <link rel="stylesheet" href="style.css">
        <!-- ALL VERSION CSS -->
        <link rel="stylesheet" href="css/versions.css">
        <!-- Responsive CSS -->
        <link rel="stylesheet" href="css/responsive.css">
        <!-- Custom CSS -->
        <link rel="stylesheet" href="css/custom.css">

        <!-- Modernizer for Portfolio -->
        <script src="js/modernizer.js"></script>

        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

    </head>

    <body class="host_version">

        <!-- LOADER -->
        <div id="preloader">
            <div class="loader-container">
                <div class="progress-br float shadow">
                    <div class="progress__item"></div>
                </div>
            </div>
        </div>
        <!-- END LOADER -->

        <!-- Start header -->
        <header class="top-navbar">
            <nav class="navbar navbar-expand-lg navbar-light bg-light fixed-top">
                <div class="container-fluid">
                    <a class="navbar-brand" href="index.php">
                        <img src="images/6.png" alt="" />
                    </a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbars-host" aria-controls="navbars-rs-food" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbars-host">
                        <ul class="navbar-nav ml-auto">
                            <li class="nav-item"><a class="nav-link" href="index.php">Home</a></li>
                            <li class="nav-item"><a class="nav-link" href="tentang.php">Tentang</a></li>
                            <li class="nav-item active"><a class="nav-link" href="forum.php">Forum</a></li>
                            <li class="nav-item"><a class="nav-link" href="blog.php">Blog</a></li>
                            <li class="nav-item"><a class="nav-link" href="dosen.php">Dosen</a></li>
                            <li class="nav-item"><a class="nav-link" href="gallery.php">Gallery</a></li>
                            <li class="nav-item"><a class="nav-link" href="kontak.php">Kontak</a></li>
                        </ul>
                        <ul class="nav navbar-nav navbar-right">
                            <?php if (!isset($_SESSION['id_alumni'])) { ?>
                                <li><a class="hover-btn-new log orange" href="authentication-login.php"><span>Login</span></a></li>
                            <?php } else { ?>
                                <li class="nav-item dropdown">
                                    <?php
                                    $id_alumni = $_SESSION['id_alumni'];
                                    $alumni = mysqli_query($con, "SELECT * FROM user WHERE id_user='$id_alumni'");
                                    $a = mysqli_fetch_array($alumni);
                                    ?>
                                    <a class="hover-btn-new log orange" href="authentication-login.php" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span><?php echo $a['nama']; ?></span></a>
                                    <div class="dropdown-menu dropdown-menu-right user-dd animated">
                                        <a class="dropdown-item" href="profile.php?id_alumni=<?php echo $id_alumni ?>">My Profile</a>
                                        <a class="dropdown-item" href="change_password.php?id_alumni=<?php echo $id_alumni ?>">Change Password</a>
                                        <div class="dropdown-divider"></div>
                                        <a class="dropdown-item" href="pages-chat.php">Message</a>
                                        <div class="dropdown-divider"></div>
                                        <?php $_SESSION['url'] = "http://fumino-furuhashi.san/global/"; ?>
                                        <a class="dropdown-item" href="config/logout.php">Logout</a>
                                    </div>
                                </li>
                            <?php } ?>
                        </ul>
                    </div>
                </div>
            </nav>
        </header>
        <!-- End header -->
        <br><br>
        <div class="all-title-box">
            <div class="container text-center">
                <h1>Forum-forum<span class="m_1">Alumni ESQ BUSINESS SCHOOL</span></h1>
                <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modalAdd">Ikuti Forum</button>
            </div>
        </div>

        <div id="overviews" class="section wb">
            <div class="container">
                <div class="section-title row text-center">
                    <div class="col-md-8 offset-md-2">
                        <p class="lead">Menuju Generasi Emas 2045</p>
                    </div>
                </div>
                <!-- end title -->

                <hr class="invis">

                <div class="row">
                    <?php
                    $forum = mysqli_query($con, "SELECT * FROM forum WHERE id_forum='$id_forum'");
                    $f = mysqli_fetch_array($forum);
                    ?>
                    <div class="container text-center">
                        <img src="<?php echo '../img/forum/' . $f['photo']; ?>" alt="" class="img-fluid"><br><br>
                    </div>
                    <div class="col-lg-12 col-md-12 col-12">
                        <div class="course-item">
                            <div class="course-br">
                                <div class="course-title">
                                    <h2>
                                        <a href="#" title="">
                                            <?php echo $f['nama']; ?>
                                        </a>
                                    </h2>

                                </div>
                                <div class="course-rating">
                                    4.5
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star-half"></i>
                                    <?php if ($f['pembuat'] == $id_alumni) { ?>
                                        <div class="text-right">
                                            <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#editForum-<?php echo $id_forum; ?>">Edit</button>
                                            <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deleteForum-<?php echo $id_forum; ?>">Delete</button>

                                        </div>
                                        <div id="editForum-<?php echo $id_forum; ?>" class="modal fade">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <form method="post" action="config/edit_forum.php?id_forum=<?php echo $id_forum; ?>" role="form" enctype="multipart/form-data">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title">Edit Forum</h5>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <div class="form-group text-center">
                                                                <img src="<?php echo '../img/forum/' . $f['photo']; ?>" class="img-fluid">
                                                            </div>
                                                            <div class="form-group">
                                                                <label>Thumbnail</label>
                                                                <input type="file" name="photo" accept="image/*">
                                                            </div>
                                                            <div class="form-group">
                                                                <label>Judul Forum</label>
                                                                <input type="text" name="nama" class="form-control" value="<?php echo $f['nama'] ?>" required>
                                                            </div>
                                                            <div class="form-group">
                                                                <label>Jenis Forum</label>
                                                                <select name="id_kategori_forum" class="custom-select" id="inputGroupSelect01">
                                                                    <?php
                                                                    $all_kategori = mysqli_query($con, "SELECT * FROM kategori_forum");
                                                                    while ($kategori = mysqli_fetch_array($all_kategori)) {
                                                                        if ($f['id_kategori_forum'] == $kategori['id_kategori_forum']) {
                                                                            ?>
                                                                            <option value="<?php echo $kategori['id_kategori_forum']; ?>" selected><?php echo $kategori['kategori_forum']; ?></option>
                                                                        <?php } else { ?>
                                                                            <option value="<?php echo $kategori['id_kategori_forum']; ?>"><?php echo $kategori['kategori_forum']; ?></option>
                                                                            <?php
                                                                        }
                                                                    }
                                                                    ?>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <div class="col-sm-6 text-right">
                                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                                                <button type="submit" id="submit" name="submit" class="btn btn-info ml-2">Update</button>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                            <!-- Modals -->
                                        </div>
                                        <div id="deleteForum-<?php echo $id_forum; ?>" class="modal fade">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title">Hapus Forum</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <div class="form-group">
                                                            <label>Apakah anda yakin ingin menghapus forum ini?</label>
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <div class="col-sm-6 text-right">
                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                                            <a href="config/delete_forum.php?id_forum=<?php echo $id_forum; ?>"><button type="submit" class="btn btn-danger">Delete</button></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- Modals -->
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="course-meta-bot">
                                <ul>
                                    <li><i class="fa fa-calendar" aria-hidden="true"></i> 20 Januari 2019</li>
                                    <li><i class="fa fa-users" aria-hidden="true"></i> 56 Peserta</li>
                                </ul>
                            </div>
                            <div class="course-meta-bot">
                                <?php
                                $chat_forum = mysqli_query($con, "SELECT * FROM chat_forum WHERE id_forum='$id_forum'");
                                while ($chat = mysqli_fetch_array($chat_forum)) {
                                    $dt = $chat['tanggal_waktu'];
                                    $dari = $chat['dari'];
                                    ?>
                                    <div class="comment-widgets scrollable">
                                        <!-- Comment Row -->
                                        <div class="d-flex flex-row comment-row m-t-0">
                                            <?php
                                            $data = mysqli_query($con, "SELECT * FROM user WHERE id_user = '$dari'");
                                            $alumni = mysqli_fetch_array($data)
                                            ?>
                                            <div class="p-2"><img src="<?php echo '../img/photo_profile/' . $alumni['photo_profile']; ?>" alt="user" width="50" class="rounded-circle"></div>
                                            <div class="comment-text w-100">
                                                <h6 class="font-medium">
                                                    <?php echo $alumni['nama']; ?>
                                                </h6>
                                                <span class="m-b-15 d-block"><?php echo $chat['chat']; ?></span>
                                                <div class="comment-footer">
                                                    <span class="text-muted float-right"><?php echo date("d/m/Y H:i:s", strtotime($dt)); ?></span>
                                                    <?php if ($chat['dari'] == $id_alumni) { ?>
                                                        <button type="button" class="btn btn-cyan btn-sm" data-toggle="modal" data-target="#editChat-<?php echo $chat['id_chat_forum']; ?>">Edit</button>
                                                        <a href="config/delete_chat_forum.php?id_chat_forum=<?php echo $chat['id_chat_forum']; ?>"><button type="button" class="btn btn-danger btn-sm">Delete</button></a>
                                                    <?php } ?>
                                                </div>
                                                <div id="editChat-<?php echo $chat['id_chat_forum']; ?>" class="modal fade">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <form method="post" action="config/edit_chat_forum.php?id_chat_forum=<?php echo $chat['id_chat_forum']; ?>" role="form">
                                                                <div class="modal-header">
                                                                    <h5 class="modal-title">Edit Chat</h5>
                                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                        <span aria-hidden="true">&times;</span>
                                                                    </button>
                                                                </div>
                                                                <div class="modal-body">
                                                                    <div class="form-group">
                                                                        <label>Chat</label>
                                                                        <textarea class="form-control" name="chat" aria-label="With textarea"><?php echo $chat['chat']; ?></textarea>
                                                                    </div>
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <div class="col-sm-6 text-right">
                                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                                                        <button type="submit" id="submit" name="submit" class="btn btn-info ml-2">Update</button>
                                                                    </div>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                    <!-- Modals -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <br>
                                <?php } ?>
                            </div>
                            <div class="course-meta-bot">
                                <form method="post" action="config/save_chat_forum.php?id_forum=<?php echo $f['id_forum']; ?>">
                                    <div class="input-group">
                                        <textarea class="form-control" name="chat" aria-label="With textarea"></textarea>
                                        <button type="submit" class="btn btn-success btn-sm">Publish</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!-- end col -->
                </div>
                <!-- end row -->
            </div>
            <!-- end container -->
        </div>
        <!-- end section -->

        <footer class="footer">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4 col-md-4 col-xs-12">
                        <div class="widget clearfix">
                            <div class="widget-title">
                                <h3>Tentang</h3>
                            </div>
                            <p> Spritual,Kreativitas dan Intelektual menjadi kekuatan ESQ Business School dalam pembentukan karakter mahasiswanya.</p>
                            <div class="footer-right">
                                <ul class="footer-links-soi">
                                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                    <li><a href="#"><i class="fa fa-github"></i></a></li>
                                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                    <li><a href="#"><i class="fa fa-dribbble"></i></a></li>
                                    <li><a href="#"><i class="fa fa-pinterest"></i></a></li>
                                </ul>
                                <!-- end links -->
                            </div>
                        </div>
                        <!-- end clearfix -->
                    </div>
                    <!-- end clearfix -->
                </div>
                <!-- end col -->

                <div class="col-lg-4 col-md-4 col-xs-12">
                    <div class="widget clearfix">
                        <div class="widget-title">
                            <h3>Information Link</h3>
                        </div>
                        <ul class="footer-links">
                            <li><a href="index.php">Home</a></li>
                            <li><a href="tentang.php">About</a></li>
                            <li><a href="forum.php">Forum</a></li>
                            <li><a href="blog.php">Blog</a></li>
                            <li><a href="dosen.php">Dosen</a></li>
                            <li><a href="gallery.php">Gallery</a></li>
                            <li><a href="kontak.php">Contact</a></li>
                        </ul>
                        <!-- end links -->
                    </div>
                    <!-- end clearfix -->
                </div>
                <!-- end col -->

                <div class="col-lg-4 col-md-4 col-xs-12">
                    <div class="widget clearfix">
                        <div class="widget-title">
                            <h3>Contact Details</h3>
                        </div>

                        <ul class="footer-links">
                            <li><a href="mailto:#">alumni165@esqbs.com</a></li>
                            <li><a href="#">www.alumniesqbs.com</a></li>
                            <li>40 jln.TB.Simatupang,Cilandak Timur,Jakarta Selatan,Jakarta.</li>
                            <li>+61 3 8376 6284</li>
                        </ul>
                        <!-- end col -->
                        <div class="col-lg-4 col-md-4 col-xs-12">
                            <div class="widget clearfix">
                                <div class="widget-title">
                                    <h3>Link Informasi </h3>
                                </div>
                                <ul class="footer-links">
                                    <li><a href="index.php">Home</a></li>
                                    <li><a href="tentang.php">About</a></li>
                                    <li><a href="forum.php">Forum</a></li>
                                    <li><a href="blog.php">Blog</a></li>
                                    <li><a href="dosen.php">Dosen</a></li>
                                    <li><a href="gallery.php">Gallery</a></li>
                                    <li><a href="kontak.php">Contact</a></li>
                                </ul>
                                <!-- end links -->
                            </div>
                            <!-- end clearfix -->
                        </div>
                        <!-- end col -->
                        <div class="col-lg-4 col-md-4 col-xs-12">
                            <div class="widget clearfix">
                                <div class="widget-title">
                                    <h3>Detail Kontak</h3>
                                </div>
                                <ul class="footer-links">
                                    <li><a href="mailto:#">alumni165@esqbs.com</a></li>
                                    <li><a href="#">www.alumniesqbs.com</a></li>
                                    <li>40 jln.TB.Simatupang,Cilandak Timur,Jakarta Selatan,Jakarta.</li>
                                    <li>+61 3 8376 6284</li>
                                </ul>
                                <!-- end links -->
                            </div>
                            <!-- end clearfix -->
                        </div>
                        <!-- end col -->

                    </div>
                    <!-- end row -->
                </div>
                <!-- end container -->
        </footer>
        <!-- end footer -->

        <div class="copyrights">
            <div class="container">
                <div class="footer-distributed">
                    <div class="footer-center">
                        <p class="footer-company-name">All Rights Reserved. &copy; 2018 <a href="#">SmartEDU</a> Design By : <a href="https://html.design/">html design</a></p>
                    </div>
                </div>
            </div>
            <!-- end container -->
        </div>
        <!-- end copyrights -->

        <a href="#" id="scroll-to-top" class="dmtop global-radius"><i class="fa fa-angle-up"></i></a>

        <!-- ALL JS FILES -->
        <script src="js/all.js"></script>
        <!-- ALL PLUGINS -->
        <script src="js/custom.js"></script>

    </body>

</html>
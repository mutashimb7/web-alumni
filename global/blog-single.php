<?php
session_start();
include 'config/koneksi.php';
$_SESSION['url'] = $url;
if (!isset($_GET['id_blog']) || $_GET['id_blog'] == null) {
    header('location:blog.php');
} else {
    $id_blog = $_GET['id_blog'];
}
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- Basic -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">   

        <!-- Mobile Metas -->
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Site Metas -->
        <title>Alumni - ESQ Business School</title> 
        <meta name="keywords" content="">
        <meta name="description" content="">
        <meta name="author" content="">

        <!-- Site Icons -->
        <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
        <link rel="apple-touch-icon" href="images/apple-touch-icon.png">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <!-- Site CSS -->
        <link rel="stylesheet" href="style.css">
        <!-- ALL VERSION CSS -->
        <link rel="stylesheet" href="css/versions.css">
        <!-- Responsive CSS -->
        <link rel="stylesheet" href="css/responsive.css">
        <!-- Custom CSS -->
        <link rel="stylesheet" href="css/custom.css">

        <!-- Modernizer for Portfolio -->
        <script src="js/modernizer.js"></script>

        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

    </head>
    <body class="host_version"> 
        <!-- LOADER -->
        <div id="preloader">
            <div class="loader-container">
                <div class="progress-br float shadow">
                    <div class="progress__item"></div>
                </div>
            </div>
        </div>
        <!-- END LOADER -->	

        <!-- Start header -->
        <header class="top-navbar">
            <nav class="navbar navbar-expand-lg navbar-light bg-light fixed-top">
                <div class="container-fluid">
                    <a class="navbar-brand" href="index.php">
                        <img src="images/6.png" alt="" />
                    </a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbars-host" aria-controls="navbars-rs-food" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbars-host">
                        <ul class="navbar-nav ml-auto">
                            <li class="nav-item"><a class="nav-link" href="index.php">Home</a></li>
                            <li class="nav-item"><a class="nav-link" href="about.php">Tentang</a></li>
                            <li class="nav-item"><a class="nav-link" href="forum.php">Forum</a></li>
                            <li class="nav-item active"><a class="nav-link" href="blog.php">Blog</a></li>
                            <li class="nav-item"><a class="nav-link" href="dosen.php">Dosen</a></li>
                            <li class="nav-item"><a class="nav-link" href="gallery.php">Gallery</a></li>
                            <li class="nav-item"><a class="nav-link" href="kontak.php">Kontak</a></li>
                        </ul>
                        <ul class="nav navbar-nav navbar-right">
                            <?php if (!isset($_SESSION['id_alumni'])) { ?>
                                <li><a class="hover-btn-new log orange" href="authentication-login.php"><span>Login</span></a></li>
                            <?php } else { ?>
                                <li class="nav-item dropdown">
                                    <?php
                                    $id_alumni = $_SESSION['id_alumni'];
                                    $alumni = mysqli_query($con, "SELECT * FROM user WHERE id_user='$id_alumni'");
                                    $a = mysqli_fetch_array($alumni);
                                    ?>
                                    <a class="hover-btn-new log orange" href="authentication-login.php" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span><?php echo $a['nama']; ?></span></a>
                                    <div class="dropdown-menu dropdown-menu-right user-dd animated">
                                        <a class="dropdown-item" href="profile.php?id_alumni=<?php echo $id_alumni ?>">My Profile</a>
                                        <a class="dropdown-item" href="change_password.php?id_alumni=<?php echo $id_alumni ?>">Change Password</a>
                                        <div class="dropdown-divider"></div>
                                        <a class="dropdown-item" href="pages-chat.php">Message</a>
                                        <div class="dropdown-divider"></div>
                                        <a class="dropdown-item" href="config/logout.php">Logout</a>
                                    </div>
                                </li>
                            <?php } ?>
                        </ul>
                    </div>
                </div>
            </nav>
        </header>
        <!-- End header -->
        <br><br>
        <div class="all-title-box">
            <div class="container text-center">
                <h1>BLOG<span class="m_1">Alumni ESQ BUSINESS SCHOOL</span></h1>
            </div>
        </div>

        <div id="overviews" class="section wb">
            <div class="container">
                <div class="row">
                    <div class="col-lg-9 blog-post-single">
                        <?php
                        $blog = mysqli_query($con, "SELECT * FROM blog WHERE id_blog='$id_blog'");
                        $b = mysqli_fetch_array($blog);
                        $dt = $b['tanggal'];
                        ?> 
                        <div class="blog-item">
                            <div class="image-blog">
                                <img src="<?php echo '../img/blog/' . $b['photo']; ?>" alt="" class="img-fluid">
                            </div>
                            <div class="post-content">
                                <div class="post-date">
                                    <span class="day"><?php echo date("d", strtotime($dt)); ?></span>
                                    <span class="month"><?php echo date("M", strtotime($dt)); ?></span>
                                </div>
                                <div class="meta-info-blog">
                                    <span><i class="fa fa-calendar"></i> <?php echo date("M d, Y", strtotime($dt)); ?> </span>
                                </div>
                                <div class="blog-title">
                                    <h2><a href="#" title=""><?php echo $b['judul']; ?>.</a></h2>
                                </div>
                                <div class="blog-desc">
                                    <p><?php echo $b['isi']; ?></p>
                                </div>							
                            </div>
                        </div>
                    </div><!-- end col -->
                    <div class="col-lg-3 col-12 right-single">
                        <div class="widget-search">
                            <div class="site-search-area">
                                <form method="get" id="site-searchform" action="#">
                                    <div>
                                        <input class="input-text form-control" name="search-k" id="search-k" placeholder="Search keywords..." type="text">
                                        <input id="searchsubmit" value="Search" type="submit">
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="widget-categories">
                            <h3 class="widget-title">Categories</h3>
                            <ul>
                                <li><a href="#">Political Science</a></li>
                                <li><a href="#">Business Leaders Guide</a></li>
                                <li><a href="#">Become a Product Manage</a></li>
                                <li><a href="#">Language Education</a></li>
                                <li><a href="#">Micro Biology</a></li>
                                <li><a href="#">Social Media Management</a></li>
                            </ul>
                        </div>
                        <div class="widget-tags">
                            <h3 class="widget-title">Search Tags</h3>
                            <ul class="tags">
                                <li><a href="#"><b>business</b></a></li>
                                <li><a href="#"><b>jquery</b></a></li>
                                <li><a href="#">corporate</a></li>
                                <li><a href="#">portfolio</a></li>
                                <li><a href="#">css3</a></li>
                                <li><a href="#"><b>theme</b></a></li>
                                <li><a href="#"><b>html5</b></a></li>
                                <li><a href="#"><b>mysql</b></a></li>
                                <li><a href="#">multipurpose</a></li>
                                <li><a href="#">responsive</a></li>
                                <li><a href="#">premium</a></li>
                                <li><a href="#">javascript</a></li>
                                <li><a href="#"><b>Best jQuery</b></a></li>
                            </ul>
                        </div>
                    </div>
                </div><!-- end row -->
            </div><!-- end container -->
        </div><!-- end section -->

        <footer class="footer">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4 col-md-4 col-xs-12">
                        <div class="widget clearfix">
                            <div class="widget-title">
                                <h3>Tentang</h3>
                            </div>
                            <p> Spritual,Kreativitas dan Intelektual menjadi kekuatan ESQ Business School dalam pembentukan karakter mahasiswanya.</p>
                            <div class="footer-right">
                                <ul class="footer-links-soi">
                                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                    <li><a href="#"><i class="fa fa-github"></i></a></li>
                                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                    <li><a href="#"><i class="fa fa-dribbble"></i></a></li>
                                    <li><a href="#"><i class="fa fa-pinterest"></i></a></li>
                                </ul>
                                <!-- end links -->
                            </div>
                        </div>
                        <!-- end clearfix -->
                    </div>
                    <!-- end col -->
                    <div class="col-lg-4 col-md-4 col-xs-12">
                        <div class="widget clearfix">
                            <div class="widget-title">
                                <h3>Link Informasi </h3>
                            </div>
                            <ul class="footer-links">
                                <li><a href="index.php">Home</a></li>
                                <li><a href="tentang.php">About</a></li>
                                <li><a href="forum.php">Forum</a></li>
                                <li><a href="blog.php">Blog</a></li>
                                <li><a href="dosen.php">Dosen</a></li>
                                <li><a href="gallery.php">Gallery</a></li>
                                <li><a href="kontak.php">Contact</a></li>
                            </ul>
                            <!-- end links -->
                        </div>
                        <!-- end clearfix -->
                    </div>
                    <!-- end col -->
                    <div class="col-lg-4 col-md-4 col-xs-12">
                        <div class="widget clearfix">
                            <div class="widget-title">
                                <h3>Detail Kontak</h3>
                            </div>
                            <ul class="footer-links">
                                <li><a href="mailto:#">alumni165@esqbs.com</a></li>
                                <li><a href="#">www.alumniesqbs.com</a></li>
                                <li>40 jln.TB.Simatupang,Cilandak Timur,Jakarta Selatan,Jakarta.</li>
                                <li>+61 3 8376 6284</li>
                            </ul><!-- end links -->
                        </div><!-- end clearfix -->
                    </div><!-- end col -->

                </div><!-- end row -->
            </div><!-- end container -->
        </footer><!-- end footer -->

        <div class="copyrights">
            <div class="container">
                <div class="footer-distributed">
                    <div class="footer-center">                   
                        <p class="footer-company-name">All Rights Reserved. &copy; 2018 <a href="#">SmartEDU</a> Design By : <a href="https://html.design/">html design</a></p>
                    </div>
                </div>
            </div><!-- end container -->
        </div><!-- end copyrights -->

        <a href="#" id="scroll-to-top" class="dmtop global-radius"><i class="fa fa-angle-up"></i></a>

        <!-- ALL JS FILES -->
        <script src="js/all.js"></script>
        <!-- ALL PLUGINS -->
        <script src="js/custom.js"></script>
    </body>
</html>
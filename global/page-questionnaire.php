<?php
session_start();
if (!isset($_SESSION['id_alumni'])) {
    header("location:authentication-login.php");
} else {
    $id_alumni = $_SESSION['id_alumni'];
    include 'config/koneksi.php';
    $alumni = mysqli_query($con, "SELECT * FROM user WHERE id_user='$id_alumni'");
    $a = mysqli_fetch_array($alumni);
}
?>
<!DOCTYPE html>
<html dir="ltr" lang="en">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Tell the browser to be responsive to screen width -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <!-- Favicon icon -->
        <link rel="icon" type="image/png" sizes="16x16" href="../assets/images/favicon.png">
        <title>Alumni - ESQBS</title>
        <!-- Custom CSS -->
        <link href="//netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
        <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script>
        <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
        <link href="css/custom-questionnaire.css" rel="stylesheet">
    </head>

    <body>
        <div class="container">
            <h2 class="text-center">LENGKAPI DATA DIRI</h2>
            <div class="stepwizard">
                <div class="stepwizard-row setup-panel">
                    <div class="stepwizard-step">
                        <a href="#step-1" type="button" class="btn btn-primary btn-circle">1</a>
                        <p>Data Diri</p>
                    </div>
                    <div class="stepwizard-step">
                        <a href="#step-2" type="button" class="btn btn-default btn-circle" disabled="disabled">2</a>
                        <p>Studi</p>
                    </div>
                    <div class="stepwizard-step">
                        <a href="#step-3" type="button" class="btn btn-default btn-circle" disabled="disabled">3</a>
                        <p>Kegiatan</p>
                    </div>
                    <div class="stepwizard-step">
                        <a href="#step-4" type="button" class="btn btn-default btn-circle" disabled="disabled">4</a>
                        <p>Pekerjaan</p>
                    </div>
                </div>
            </div>
            <form method="post" action="config/save_questionnaire.php" role="form">
                <div class="row setup-content" id="step-1">
                    <div class="col-xs-12">
                        <div class="col-md-12">
                            <h3>Data Diri</h3>
                            <div class="form-group">
                                <label for="name">Nama Lengkap</label>
                                <input id="name" name="nama" type="text" placeholder="nama lengkap" class="form-control" value="<?php echo $a['nama']; ?>" >
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="namaDepan">Tempat Lahir</label>
                                        <input type="text" class="form-control" name="tempat_lahir" placeholder="tempat lahir" id="tempat_lahir" >
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="tanggalLahir">Tanggal Lahir</label>
                                        <div class="row ">
                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <select name="tanggal" class="form-control custom-select" >
                                                        <option value="">Tanggal</option>
                                                        <?php
                                                        $tanggal = 01;
                                                        while ($tanggal < 32) {
                                                            ?>
                                                            <option value="<?php echo $tanggal; ?>"><?php echo $tanggal++; ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <select name="bulan" class="form-control custom-select" >
                                                        <option value="">Bulan</option>
                                                        <?php
                                                        $bulan = 01;
                                                        while ($bulan < 13) {
                                                            ?>
                                                            <option value="<?php echo $bulan; ?>"><?php echo $bulan++; ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <select name="tahun" class="form-control custom-select" >
                                                        <option value="">Tahun</option>
                                                        <?php
                                                        $tahun = 1990;
                                                        while ($tahun < 2011) {
                                                            ?>
                                                            <option value="<?php echo $tahun; ?>"><?php echo $tahun++; ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="jenis_kelamin">Jenis Kelamin</label>
                                        <select name="jenis_kelamin" class="form-control custom-select" >
                                            <option value="">Jenis Kelamin</option>
                                            <option value="Laki-laki">Laki-laki</option>
                                            <option value="Perempuan">Perempuan</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="status_perkawinan">Status Perkawinan</label>
                                        <select name="status_perkawinan" class="form-control custom-select" >
                                            <option value="">Status Perkawinan</option>
                                            <option value="Belum Menikah">Belum Menikah</option>
                                            <option value="Menikah">Menikah</option>
                                            <option value="Janda/Duda">Janda/Duda</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="nomor_kontak">Nomor Telepon</label>
                                        <input type="text" name="nomor_kontak" class="form-control" data-mask="0000-0000-0000" data-mask-clearifnotmatch="true" placeholder="0000-0000-0000"  />
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="email">Email</label>
                                        <input id="email" name="email" type="text" placeholder="email" class="form-control" value="<?php echo $a['email']; ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="alamat">Alamat</label>
                                <textarea class="form-control" type="textarea" id="alamat" name="alamat" placeholder="alamat" maxlength="140" rows="3" ></textarea>
                            </div>
                            <br>
                            <h3>Data Pekerjaan</h3>
                            <div class="form-group">
                                <label for="nama_perusahaan">Nama Perusahaan/Organisasi tempat bekerja</label>
                                <input id="nama_perusahaan" name="nama_perusahaan" type="text" placeholder="nama perusahaan/organisasi" class="form-control" >
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="nama_leader">Nama Leader Tempat Kerja Saat Ini</label>
                                        <input id="nama_leader" name="nama_leader" type="text" placeholder="nama leader" class="form-control" >
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="jabatan">Jabatan</label>
                                        <select name="jabatan" class="form-control custom-select" >
                                            <option value="">Jabatan</option>
                                            <option value="Ketua Cabang">Ketua Cabang</option>
                                            <option value="Manager">Manager</option>
                                            <option value="Bos Perusahaan">Bos Perusahaan</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="kontak_kantor">Nomor Telepon Kantor</label>
                                        <input type="text" name="kontak_kantor" class="form-control" data-mask="0000-0000-0000" data-mask-clearifnotmatch="true" placeholder="0000-0000-0000"  />
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="email_kantor">Email Kantor</label>
                                        <input id="email_kantor" name="email_kantor" type="text" placeholder="email" class="form-control" >
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="alamat_kantor">Alamat Kantor</label>
                                <textarea class="form-control" type="textarea" id="alamat_kantor" name="alamat_kantor" placeholder="alamat kantor" maxlength="140" rows="3"></textarea>
                            </div>
                            <button class="btn btn-primary nextBtn btn-lg pull-right" type="button" >Next</button>
                        </div>
                    </div>
                </div><br>
                <div class="row setup-content" id="step-2">
                    <div class="col-xs-12">
                        <div class="col-md-12">
                            <h3>Data Studi</h3>
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="nama_sma">Data SMA</label>
                                        <input id="nama_sma" name="nama_sma" type="text" class="form-control" >
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <label for="tahun_masuk sma">Tahun Masuk</label>
                                        <select name="tahun_masuk_sma" class="form-control custom-select" >
                                            <option value="">Tahun Masuk</option>
                                            <?php
                                            $tahun = 2000;
                                            while ($tahun < 2011) {
                                                ?>
                                                <option value="<?php echo $tahun; ?>"><?php echo $tahun++; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <label for="tahun_lulus_sma">Tahun Lulus</label>
                                        <select name="tahun_lulus_sma" class="form-control custom-select" >
                                            <option value="">Tahun Lulus</option>
                                            <?php
                                            $tahun = 2000;
                                            while ($tahun < 2011) {
                                                ?>
                                                <option value="<?php echo $tahun; ?>"><?php echo $tahun++; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="jurusan_sma">Jurusan</label>
                                        <select name="jurusan_sma" class="form-control custom-select" >
                                            <option value="">Jurusan</option>
                                            <option value="IPA">IPA</option>
                                            <option value="IPS">IPS</option>
                                            <option value="Bahasa">Bahasa</option>
                                            <option value="Agama">Agama</option>
                                            <option value="Kejuruan">Kejuruan</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="nama_s1">Data S1</label>
                                        <input id="nama_s1" name="nama_s1" type="text" class="form-control" value="STMIK ESQ Business School" >
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <label for="tahun_masuk_s1">Tahun Masuk</label>
                                        <select name="tahun_masuk_s1" class="form-control custom-select" >
                                            <option value="">Tahun Masuk</option>
                                            <?php
                                            $tahun = 2013;
                                            while ($tahun < 2040) {
                                                ?>
                                                <option value="<?php echo $tahun; ?>"><?php echo $tahun++; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <label for="tahun_lulus_s1">Tahun Lulus</label>
                                        <select name="tahun_lulus_s1" class="form-control custom-select" >
                                            <option value="">Tahun Lulus</option>
                                            <?php
                                            $y_now = date('Y');
                                            $tahun = 2017;
                                            while ($tahun < 2040) {
                                                if ($y_now == $tahun) {
                                                    ?>
                                                    <option selected value="<?php echo $tahun; ?>"><?php echo $tahun++; ?></option>
                                                <?php } else { ?>
                                                    <option value="<?php echo $tahun; ?>"><?php echo $tahun++; ?></option>
                                                <?php }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <label for="program_studi">Program Studi</label>
                                        <select name="program_studi" class="form-control custom-select" >
                                            <option value="">Program Studi</option>
                                            <option value="Manajemen Bisnis Konvensional">Manajemen Bisnis Konvensional</option>
                                            <option value="Manajemen Bisnis Syari'ah">Manajemen Bisnis Syari'ah</option>
                                            <option value="Sistem Informasi">Sistem Informasi</option>
                                            <option value="Ilmu Komputer">Ilmu Komputer</option>
                                            <option value="Second Generation">Second Generation</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <label for="ipk">IPK</label>
                                        <select name="ipk" class="form-control custom-select" >
                                            <option value="">IPK</option>
                                            <option value="A">> 3.50</option>
                                            <option value="B">3.00 - 3.50</option>
                                            <option value="C">2.75 - 3.00</option>
                                            <option value="D">2.00 - 2.75</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="judul_skripsi">Judul Skripsi</label> 
                                <input id="judul_skripsi" name="judul" type="text" class=" form-control" >
                            </div>
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="pengalaman_akademik">Pengalaman Akademik</label> 
                                        <div class="form-check">
                                            <div class="checkbox">
                                                <label><input type="checkbox" name="pengalaman_akademik_Asisten_Dosen" value="Asisten Dosen">Asisten Dosen</label>
                                            </div>
                                            <div class="checkbox">
                                                <label><input type="checkbox" name="pengalaman_akademik_Membantu_Penelitian_Dosen" value="Membantu Penelitian Dosen">Membantu Penelitian Dosen</label>
                                            </div>
                                            <div class="checkbox">
                                                <label><input type="checkbox" name="pengalaman_akademik_Penelitian_Mandiri" value="Penelitian Mandiri">Penelitian Mandiri</label>
                                            </div>
                                            <div class="checkbox">
                                                <label><input type="checkbox" name="pengalaman_akademik_Publikasi_Ilmiah" value="Publikasi Ilmiah">Publikasi Ilmiah</label>
                                            </div>
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <div class="input-group-addon">Lainnya</div>
                                                    <input type="text" name="pengalaman_akademik_Lainnya" class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="pendidikan_tambahan">Pendidikan Tambahan Selama Kuliah</label>
                                        <div class="form-check">
                                            <div class="checkbox">
                                                <label><input type="checkbox" name="pendidikan_tambahan_Kursus_Bahasa_Asing" value="Kursus Bahasa Asing">Kursus Bahasa Asing</label>
                                            </div>
                                            <div class="checkbox">
                                                <label><input type="checkbox" name="pendidikan_tambahan_Training_Kepemimpinan/Manajemen" value="Training Kepemimpinan/Manajemen">Training Kepemimpinan/Manajemen</label>
                                            </div>
                                            <div class="checkbox">
                                                <label><input type="checkbox" name="pendidikan_tambahan_Kursus_Komputer" value="Kursus Komputer">Kursus Komputer</label>
                                            </div>
                                            <div class="checkbox">
                                                <label><input type="checkbox" name="pendidikan_tambahan_Training_Keterampilan_Praktis_Sebidang" value="Training Keterampilan Praktis Sebidang">Training Keterampilan Praktis Sebidang</label>
                                            </div>
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <div class="input-group-addon">Lainnya</div>
                                                    <input type="text" class="form-control" name="pendidikan_tambahan_Lainnya">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="pendidikan_tambahan">Pendidikan Tambahan Setelah Kuliah</label> 
                                        <div class="form-check">
                                            <div class="checkbox">
                                                <label><input type="checkbox" name="pendidikan_tambahan_Kursus_Bahasa_Asing" value="Kursus Bahasa Asing">Kursus Bahasa Asing</label>
                                            </div>
                                            <div class="checkbox">
                                                <label><input type="checkbox" name="pendidikan_tambahan_Training_Kepemimpinan/Manajemen" value="Training Kepemimpinan/Manajemen">Training Kepemimpinan/Manajemen</label>
                                            </div>
                                            <div class="checkbox">
                                                <label><input type="checkbox" name="pendidikan_tambahan_Kursus_Komputer" value="Kursus Komputer">Kursus Komputer</label>
                                            </div>
                                            <div class="checkbox">
                                                <label><input type="checkbox" name="pendidikan_tambahan_Training_Keterampilan_Praktis_Sebidang" value="Training Keterampilan Praktis Sebidang">Training Keterampilan Praktis Sebidang</label>
                                            </div>
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <div class="input-group-addon">Lainnya</div>
                                                    <input type="text" class="form-control" name="pendidikan_tambahan_Lainnya">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <button class="btn btn-primary nextBtn btn-lg pull-right" type="button" >Next</button>
                        </div>
                    </div>
                </div>
                <div class="row setup-content" id="step-3">
                    <div class="col-xs-12">
                        <div class="col-md-12">
                            <h3>Kegiatan</h3>
                            <div class="form-group">
                                <label for="aktivitas">Aktivitas Kemahasiswaan</label>
                                <div class="form-check">
                                    <div class="checkbox">
                                        <label><input type="checkbox" name="aktivitas_kemahasiswaan_Menjadi_Pengurus_Unit_Aktivitas_Mahasiswa_di_Tingkat_Universitas" value="Menjadi Pengurus Unit Aktivitas Mahasiswa di Tingkat Universitas">Menjadi Pengurus Unit Aktivitas Mahasiswa di Tingkat Universitas</label>
                                    </div>
                                    <div class="checkbox">
                                        <label><input type="checkbox" name="aktivitas_kemahasiswaan_Menjadi_Pengurus_Unit_Aktivitas_Mahasiswa_di_Tingkat_Jurusan/Profesi" value="Menjadi Pengurus Unit Aktivitas Mahasiswa di Tingkat Jurusan/Profesi">Menjadi Pengurus Unit Aktivitas Mahasiswa di Tingkat Jurusan/Profesi</label>
                                    </div>
                                    <div class="checkbox">
                                        <label><input type="checkbox" name="aktivitas_kemahasiswaan_Menjadi_Pengurus_Organisasi_Masyarakat_di_Luar_Kampus" value="Menjadi Pengurus Organisasi Masyarakat di Luar Kampus">Menjadi Pengurus Organisasi Masyarakat di Luar Kampus</label>
                                    </div>
                                    <div class="checkbox">
                                        <label><input type="checkbox" name="aktivitas_kemahasiswaan_Training_Keterampilan_Praktis_Sebidang" value="Training Keterampilan Praktis Sebidang">Training Keterampilan Praktis Sebidang</label>
                                    </div>
                                    <div class="checkbox">
                                        <label><input type="checkbox" name="aktivitas_kemahasiswaan_Tidak_Menjadi_Aktivis" value="Tidak Menjadi Aktivis">Tidak Menjadi Aktivis</label>
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-addon">Lainnya</div>
                                            <input type="text" name="aktivitas_kemahasiswaan_Lainnya" class="form-control">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <button class="btn btn-primary nextBtn btn-lg pull-right" type="button" >Next</button>
                        </div>
                    </div>
                </div>
                <div class="row setup-content" id="step-4">
                    <div class="col-xs-12">
                        <div class="col-md-12">
                            <h3>Pekerjaan</h3>
                            <div class="form-group">
                                <label for="bekerja">Apakah anda sudah bekerja sebelum lulus ?</label>
                                <div class="form-check">
                                    <div class="radio">
                                        <label><input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">Ya, di lembaga yang sesuai dengan bidang yang ditekuni</label>
                                    </div>
                                    <div class="radio">
                                        <label><input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">Ya, di lembaga yang tidak sesuai dengan bidang yang ditekuni</label>
                                    </div>
                                    <div class="radio">
                                        <label><input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">Tidak</label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="mencarikerja">Kapan anda mulai mencari pekerjaan ? </label>
                                <div class="form-check">
                                    <div class="radio">
                                        <label><input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">Saya tidak mencari kerja (Langsung ke pertanyaan ke no.23)</label>
                                    </div>
                                    <div class="radio">
                                        <label><input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">Saya membangun bisnis sendiri (Langsung ke pertanyaan ke no. 28)</label>
                                    </div>
                                    <div class="radio">
                                        <label><input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <div class="input-group-addon">Kira- kira</div>
                                                    <input type="text" class="form-control" id="exampleInputAmount" placeholder="Bulan">
                                                    <div class="input-group-addon">Bulan Sebelum Lulus</div>
                                                </div>
                                            </div>
                                        </label>
                                    </div>
                                    <div class="radio">
                                        <label><input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <div class="input-group-addon">Kira- kira</div>
                                                    <input type="text" class="form-control" id="exampleInputAmount" placeholder="Bulan">
                                                    <div class="input-group-addon">Bulan Setelah Lulus</div>
                                                </div>
                                            </div>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="mencarikerja">Kapan anda mulai mencari pekerjaan ?</label>
                                <div class="form-check">
                                    <div class="checkbox">
                                        <label><input type="checkbox" value="">Melalui iklan di koran/majalah/brosur</label>
                                    </div>
                                    <div class="checkbox">
                                        <label><input type="checkbox" value="">Melamar ke perusahaan tanpa mengetahui ada lowongan yang ada</label>
                                    </div>
                                    <div class="checkbox">
                                        <label><input type="checkbox" value="">Pergi ke bursa/pameran kerja</label>
                                    </div>
                                    <div class="checkbox">
                                        <label><input type="checkbox" value="">Mencari lewat internet/iklan online</label>
                                    </div>
                                    <div class="checkbox">
                                        <label><input type="checkbox" value="">Melalui Kemenakertrans</label>
                                    </div>
                                    <div class="checkbox">
                                        <label><input type="checkbox" value="">Dihubungi perusahaan</label>
                                    </div>
                                    <div class="checkbox">
                                        <label><input type="checkbox" value="">Melalui agen tenaga kerja komersial /swasta (outsource)</label>
                                    </div>
                                    <div class="checkbox">
                                        <label><input type="checkbox" value="">Melalui penempatan kerja / magang</label>
                                    </div>
                                    <div class="checkbox">
                                        <label><input type="checkbox" value="">Melalui informasi dari pusat karir atau hubungan alumni STIMIK ESQ</label>
                                    </div>
                                    <div class="checkbox">
                                        <label><input type="checkbox" value="">Membangun network sejak masih kuliah</label>
                                    </div>
                                    <div class="checkbox">
                                        <label><input type="checkbox" value="">Melalui staff / dosen pembimbing</label>
                                    </div>
                                    <div class="checkbox">
                                        <label><input type="checkbox" value="">Melalui orang tua/keluarga</label>
                                    </div>
                                    <div class="checkbox">
                                        <label><input type="checkbox" value="">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <div class="input-group-addon">Lainnya</div>
                                                    <input type="text" class="form-control" id="exampleInputAmount">
                                                </div>
                                            </div>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="mencarikerja">Berapa bulan waktu yang dihabiskan (sebelum dan sesudah kelulusan) untuk
                                    memperoleh pekerjaan pertama ? </label>
                                <div class="form-check">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-addon">Kira- kira</div>
                                            <input type="text" class="form-control" id="exampleInputAmount" placeholder="Bulan">
                                            <div class="input-group-addon">Bulan Setelah Lulus</div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-addon">Kira- kira</div>
                                            <input type="text" class="form-control" id="exampleInputAmount" placeholder="Bulan">
                                            <div class="input-group-addon">Bulan Setelah Lulus</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="name">Berapa banyak perusahaan/instansi/institusi yang sudah anda lamar (lewat surat/e-mail) sebelum anda memperoleh pekerjaan pertama ? *jika tidak mencari lowongan, isi dengan angka 0)</label>
                                <input id="name" name="name" type="text" placeholder="nama lengkap" class=" form-control">
                            </div>
                            <div class="form-group">
                                <label for="name">Berapa banyak perusahaan/instansi/institusi yang merespon lamaran anda ? *jika tidak ada lowongan yang merespon, isi dengan angka 0)</label>
                                <input id="name" name="name" type="text" placeholder="nama lengkap" class=" form-control">
                            </div>
                            <div class="form-group">
                                <label for="name">Berapa banyak perusahaan/instansi/institusi yang mengundan anda untuk ujian / wawancara penerimaan karyawan ? *jika tidak pernah mengikuti ujian / wawancara, isi dengan angka 0)</label>
                                <input id="name" name="name" type="text" placeholder="nama lengkap" class=" form-control">
                            </div>
                            <div class="form-group">
                                <label for="bekerja">Apakah anda bekerja saat ini (termasuk kerja sambilan dan wirausaha) ?</label>
                                <div class="form-check">
                                    <div class="radio">
                                        <label><input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">Ya (Langsung ke pertanyaa)</label>
                                    </div>
                                    <div class="radio">
                                        <label><input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">Tidak</label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="aktivitas">Bagaimana anda menggambarkan situasi anda saat ini ?</label>
                                <div class="form-check">
                                    <div class="checkbox">
                                        <label><input type="checkbox" value="">Saya masih belajar/melanjutkan kuliah pasca sarjana</label>
                                    </div>
                                    <div class="checkbox">
                                        <label><input type="checkbox" value="">Saya sudah menikah</label>
                                    </div>
                                    <div class="checkbox">
                                        <label><input type="checkbox" value="">Saya sibuk dengan keluarga dan anak-anak</label>
                                    </div>
                                    <div class="checkbox">
                                        <label><input type="checkbox" value="">Saya sekarang sedang mencari pekerjaan</label>
                                    </div>
                                    <div class="checkbox">
                                        <label><input type="checkbox" value="">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <div class="input-group-addon">Lainnya</div>
                                                    <input type="text" class="form-control" id="exampleInputAmount">
                                                </div>
                                            </div>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="bekerja">Apakah anda bekerja saat ini (termasuk kerja sambilan dan wirausaha) ?</label>
                                <div class="form-check">
                                    <div class="radio">
                                        <label><input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">Tidak</label>
                                    </div>
                                    <div class="radio">
                                        <label><input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">Tidak , tapi saya sedang menunggu hasil lamaran kerja</label>
                                    </div>
                                    <div class="radio">
                                        <label><input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">Ya, saya akan mulai kerja dalam 2 minggu kedepan</label>
                                    </div>
                                    <div class="radio">
                                        <label><input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">Ya, tapi saya belum pasti akan bekerja dalam 2 minggu kedepan</label>
                                    </div>
                                    <div class="radio">
                                        <label><input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <div class="input-group-addon">Lainnya</div>
                                                    <input type="text" class="form-control" id="exampleInputAmount">
                                                </div>
                                            </div>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="bekerja">Apakah perusahaan/instansi tempat anda bekerja saat ini merupakan pekerjaan pertama anda ?</label>
                                <div class="form-check">
                                    <div class="radio">
                                        <label><input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">Ya</label>
                                    </div>
                                    <div class="radio">
                                        <label><input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">Tidak</label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="bekerja">Berapakah gaji pertama anda bekerja ?</label>
                                <div class="form-check">
                                    <div class="radio">
                                        <label><input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">Kurang dari Rp.1.000.000,-</label>
                                    </div>
                                    <div class="radio">
                                        <label><input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">Antara Rp.1.000.000,- - Rp.3.000.000,-</label>
                                    </div>
                                    <div class="radio">
                                        <label><input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">Antara Rp.1.000.000,- - Rp.3.000.000,-</label>
                                    </div>
                                    <div class="radio">
                                        <label><input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">Antara Rp.3.000.000,- - Rp.5.000.000,-</label>
                                    </div>
                                    <div class="radio">
                                        <label><input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">Antara Rp.5.000.000,- - Rp.7.500.000,-</label>
                                    </div>
                                    <div class="radio">
                                        <label><input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">Antara Rp.7.500.000,- - Rp.10.000.000,-</label>
                                    </div>
                                    <div class="radio">
                                        <label><input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">Antara Rp.10.000.000,- - Rp.12.500.000,-</label>
                                    </div>
                                    <div class="radio">
                                        <label><input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">Antara Rp.12.500.000,- - Rp.15.000.000,-</label>
                                    </div>
                                    <div class="radio">
                                        <label><input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">Lebih dari Rp.15.000.000,-</label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="bekerja">28. Apakah jenis pekerjaan anda saat ini ?</label>
                                <div class="form-check">
                                    <div class="radio">
                                        <label><input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">Instansi pemerintahan (termasuk BUMN)</label>
                                    </div>
                                    <div class="radio">
                                        <label><input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">Organisasi non protif / Lembaga Swadaya Masyarakat</label>
                                    </div>
                                    <div class="radio">
                                        <label><input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">Perusahaan Swasta</label>
                                    </div>
                                    <div class="radio">
                                        <label><input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">Perusahaan Multinasional</label>
                                    </div>
                                    <div class="radio">
                                        <label><input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">Wiraswasta / Perusahaan sendiri</label>
                                    </div>
                                    <div class="radio">
                                        <label><input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <div class="input-group-addon">Lainnya</div>
                                                    <input type="text" class="form-control" id="exampleInputAmount">
                                                </div>
                                            </div>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="name">Apa posisi kerja / Jabatan anda saat ini ?</label>
                                <input id="name" name="name" type="text" placeholder="nama lengkap" class=" form-control">
                            </div>
                            <div class="form-group">
                                <label for="bekerja">Seberapa erat hubungan bidang studi dengan pekerjaan anda ?</label>
                                <div class="form-check">
                                    <div class="radio">
                                        <label><input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">Sangat Erat</label>
                                    </div>
                                    <div class="radio">
                                        <label><input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">Erat</label>
                                    </div>
                                    <div class="radio">
                                        <label><input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">Cukup Erat</label>
                                    </div>
                                    <div class="radio">
                                        <label><input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">Kurang Erat</label>
                                    </div>
                                    <div class="radio">
                                        <label><input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">Tidak ada hubungan sama sekali</label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="bekerja">Tingkat pendidikan apa yang paling tepat untuk anda saat ini ?</label>
                                <div class="form-check">
                                    <div class="radio">
                                        <label><input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">Setingkat Lebih Tinggi</label>
                                    </div>
                                    <div class="radio">
                                        <label><input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">Tingkat Yang Sama</label>
                                    </div>
                                    <div class="radio">
                                        <label><input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">Setingkat Lebih Rendah</label>
                                    </div>
                                    <div class="radio">
                                        <label><input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">Tidak Perlu Pendidikan Tinggi</label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="bekerja">Bagaimana status Pekerjaan anda saat ini ?</label>
                                <div class="form-check">
                                    <div class="radio">
                                        <label><input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">Tenaga tetap</label>
                                    </div>
                                    <div class="radio">
                                        <label><input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">Honorer / Freelancer</label>
                                    </div>
                                    <div class="radio">
                                        <label><input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">Kontrak penuh waktu (fulltime)</label>
                                    </div>
                                    <div class="radio">
                                        <label><input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">Kontrak paruh waktu (parttime)</label>
                                    </div>
                                    <div class="radio">
                                        <label><input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <div class="input-group-addon">Lainnya</div>
                                                    <input type="text" class="form-control" id="exampleInputAmount">
                                                </div>
                                            </div>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="mencarikerja">Berapa bulan waktu yang dihabiskan (sebelum dan sesudah kelulusan) untuk
                                    memperoleh pekerjaan pertama ? </label>
                                <div class="form-check">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-addon">Dari Pekerjaan Utama Rp.</div>
                                            <input type="text" class="form-control" id="exampleInputAmount" placeholder="Bulan">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-addon">Dari Lembur dan Tips Rp.</div>
                                            <input type="text" class="form-control" id="exampleInputAmount" placeholder="Bulan">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-addon">Dari Pekerjaan Lainnya Rp.</div>
                                            <input type="text" class="form-control" id="exampleInputAmount" placeholder="Bulan">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="bekerja">Bagaimana status Pekerjaan anda saat ini ?</label>
                                <div class="form-check">
                                    <div class="radio">
                                        <label><input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">Belum Pernah</label>
                                    </div>
                                    <div class="radio">
                                        <label><input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">1 Kali</label>
                                    </div>
                                    <div class="radio">
                                        <label><input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">2 Kali</label>
                                    </div>
                                    <div class="radio">
                                        <label><input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">> 2 Kali</label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="bekerja">Apakah alasan anda pindah pekerjaan / profesi ?</label>
                                <div class="form-check">
                                    <div class="radio">
                                        <label><input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">Perbaikan Gaji</label>
                                    </div>
                                    <div class="radio">
                                        <label><input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">Kesesuaian dengan Bidang Ilmu</label>
                                    </div>
                                    <div class="radio">
                                        <label><input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">Manajemen tempat bekerja</label>
                                    </div>
                                    <div class="radio">
                                        <label><input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">Peluang karir</label>
                                    </div>
                                    <div class="radio">
                                        <label><input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">Lingkungan pekerjaan tidak sesuai</label>
                                    </div>
                                    <div class="radio">
                                        <label><input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <div class="input-group-addon">Lainnya</div>
                                                    <input type="text" class="form-control" id="exampleInputAmount">
                                                </div>
                                            </div>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="mencarikerja">Jika menurut anda pekerjaan anda saat ini tidak sesuai dengan pendidikan anda mengapa anda mengambilnya ?</label>
                                <div class="form-check">
                                    <div class="checkbox">
                                        <label><input type="checkbox" value="">Saya masih belajar/melanjutkan kuliah pasca sarjana</label>
                                    </div>
                                    <div class="checkbox">
                                        <label><input type="checkbox" value="">Pertanyaan tidak sesuai, pekerjaan sudan sesuai dengan pendidikan saya</label>
                                    </div>
                                    <div class="checkbox">
                                        <label><input type="checkbox" value="">Saya belum mendapatkan pekerjaan yang lebih sesuai</label>
                                    </div>
                                    <div class="checkbox">
                                        <label><input type="checkbox" value="">Dipekerjaan ini saya memperoleh prospek yang lebih baik</label>
                                    </div>
                                    <div class="checkbox">
                                        <label><input type="checkbox" value="">Saya lebih suka bekerja di area pekerjaan yang tidak ada hubungannya dengan pendidikan saya</label>
                                    </div>
                                    <div class="checkbox">
                                        <label><input type="checkbox" value="">Saya dipromosikan ke posisi yang kurang berhubungan dengan pendidikan saya dibanding posisi sebelumnya</label>
                                    </div>
                                    <div class="checkbox">
                                        <label><input type="checkbox" value="">Saya mendapatkan pendapatan yang lebih tinggi di pekerjaan ini</label>
                                    </div>
                                    <div class="checkbox">
                                        <label><input type="checkbox" value="">Pekerjaan saya saat ini lebih aman/secure/terjamin</label>
                                    </div>
                                    <div class="checkbox">
                                        <label><input type="checkbox" value="">Pekerjaan saya saat ini lebih menarik</label>
                                    </div>
                                    <div class="checkbox">
                                        <label><input type="checkbox" value="">Pekerjaan saya saat ini lebih memungkinkan saya mengambil pekerjaan sampingan / jadwal fleksibel</label>
                                    </div>
                                    <div class="checkbox">
                                        <label><input type="checkbox" value="">Pekerjaan saya saat ini lebih dekat dengan rumah saya</label>
                                    </div>
                                    <div class="checkbox">
                                        <label><input type="checkbox" value="">Pekerjaan saya saat ini lebih menjamin kebutuhan keluarga saya</label>
                                    </div>
                                    <div class="checkbox">
                                        <label><input type="checkbox" value="">Pada awal meniti karir ini, saya harus menerima pekerjaan yang tidak sesuai dengan pendidikan saya</label>
                                    </div>
                                    <div class="checkbox">
                                        <label><input type="checkbox" value="">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <div class="input-group-addon">Lainnya</div>
                                                    <input type="text" class="form-control" id="exampleInputAmount">
                                                </div>
                                            </div>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="text-right">
                                <button class="btn btn-success btn-lg" name="submit" type="submit">Finish!</button>
                            </div>
                        </div>
                    </div>
                </div><br>
            </form>
        </div>
    </body>
    <script src="js/custom-questionnaire.js"></script>
</html>

<?php
session_start();
include 'config/koneksi.php';
$_SESSION['url'] = "http://fumino-furuhashi.san/global/";
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- Basic -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">   

        <!-- Mobile Metas -->
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Site Metas -->
        <title>Alumni - ESQ Business School</title> 
        <meta name="keywords" content="">
        <meta name="description" content="">
        <meta name="author" content="">

        <!-- Site Icons -->
        <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
        <link rel="apple-touch-icon" href="images/apple-touch-icon.png">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <!-- Site CSS -->
        <link rel="stylesheet" href="style.css">
        <!-- ALL VERSION CSS -->
        <link rel="stylesheet" href="css/versions.css">
        <!-- Responsive CSS -->
        <link rel="stylesheet" href="css/responsive.css">
        <!-- Custom CSS -->
        <link rel="stylesheet" href="css/custom.css">

        <!-- Modernizer for Portfolio -->
        <script src="vendor/js/modernizer.js"></script>

        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

    </head>
    <body class="host_version"> 
        <!-- LOADER -->
        <div id="preloader">
            <div class="loader-container">
                <div class="progress-br float shadow">
                    <div class="progress__item"></div>
                </div>
            </div>
        </div>
        <!-- END LOADER -->	

        <!-- Start header -->
        <header class="top-navbar">
            <nav class="navbar navbar-expand-lg navbar-light bg-light fixed-top">
                <div class="container-fluid">
                    <a class="navbar-brand" href="index.php">
                        <img src="images/logo.png" alt="" />
                    </a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbars-host" aria-controls="navbars-rs-food" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbars-host">
                        <ul class="navbar-nav ml-auto">
                            <li class="nav-item"><a class="nav-link" href="index.php">Home</a></li>
                            <li class="nav-item"><a class="nav-link" href="about.php">Tentang</a></li>
                            <li class="nav-item"><a class="nav-link" href="forum.php">Forum</a></li>
                            <li class="nav-item"><a class="nav-link" href="blog.php">Blog</a></li>
                            <li class="nav-item"><a class="nav-link" href="dosen.php">Dosen</a></li>
                            <li class="nav-item"><a class="nav-link" href="gallery.php">Gallery</a></li>
                            <li class="nav-item"><a class="nav-link" href="kontak.php">Kontak</a></li>
                        </ul>
                        <ul class="nav navbar-nav navbar-right">
                            <?php if (!isset($_SESSION['id_alumni'])) { ?>
                                <li><a class="hover-btn-new log orange" href="authentication-login.php"><span>Login</span></a></li>
                            <?php } else { ?>
                                <li class="nav-item dropdown">
                                    <?php
                                    $id_alumni = $_SESSION['id_alumni'];
                                    $alumni = mysqli_query($con, "SELECT * FROM user WHERE id_user='$id_alumni'");
                                    $a = mysqli_fetch_array($alumni);
                                    ?>
                                    <a class="hover-btn-new log orange" href="authentication-login.php" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span><?php echo $a['nama']; ?></span></a>
                                    <div class="dropdown-menu dropdown-menu-right user-dd animated">
                                        <a class="dropdown-item" href="profile.php?id_alumni=<?php echo $id_alumni ?>">My Profile</a>
                                        <a class="dropdown-item" href="change_password.php?id_alumni=<?php echo $id_alumni ?>">Change Password</a>
                                        <div class="dropdown-divider"></div>
                                        <a class="dropdown-item" href="pages-chat.php">Message</a>
                                        <div class="dropdown-divider"></div>
                                        <a class="dropdown-item" href="config/logout.php">Logout</a>
                                    </div>
                                </li>
                            <?php } ?>
                        </ul>
                    </div>
                </div>
            </nav>
        </header>
        <!-- End header -->
        <br><br>
        <div class="all-title-box">
            <div class="container text-center">
                <h1>PERSONAL PROFILE<span class="m_1">Alumni ESQ BUSINESS SCHOOL</span></h1>
            </div>
        </div>

        <div id="overviews" class="section wb">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4 blog-post-single">
                        <div class="blog-item">
                            <div class="image-blog">
                                <img src="<?php echo '../img/photo_profile/' . $a['photo_profile']; ?>" class="img-fluid">
                            </div>
                            <div class="post-content text-center">
                                <br>
                                <button type="button" class="btn btn-info" data-toggle="modal" data-target="#modalUpdateFoto">Update Foto</button>
                            </div>
                        </div>
                    </div><!-- end col -->
                    <div class="col-lg-8 blog-post-single">
                        <div class="blog-item">
                            <div class="post-content">
                                <form class="form-horizontal" method="post" action="config/edit_profile.php?id_alumni=<?php echo $a['id_user'] ?>">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label for="username">ID Alumni</label>
                                                    <input type="text" class="form-control" id="username" name="username" value="<?php echo $a['id_user']; ?>" readonly="readonly">
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label for="email">Email</label>
                                                    <input type="email" class="form-control" name="email" placeholder="email" id="email" value="<?php echo $a['email'] ?>">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <label for="namaDepan">Nama Lengkap</label>
                                                    <input type="text" class="form-control" name="nama" placeholder="nama lengkap" id="nama" value="<?php echo $a['nama']; ?>">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <label for="namaDepan">Tempat Lahir</label>
                                                    <input type="text" class="form-control" name="tempat_lahir" placeholder="tempat lahir" id="tempat_lahir" value="<?php echo $a['tempat_lahir']; ?>">
                                                </div>
                                            </div>
                                            <div class="col-sm-8">
                                                <label for="tanggalLahir">Tanggal Lahir</label>
                                                <div class="row ">
                                                    <div class="col-sm-4">
                                                        <div class="input-group mb-2">
                                                            <div class="input-group-prepend">
                                                                <div class="input-group-text">D</div>
                                                            </div>
                                                            <select name="tanggal" class="form-control custom-select">
                                                                <?php
                                                                $tanggal = 01;
                                                                $d_now = date('d', strtotime($a['tanggal_lahir']));
                                                                while ($tanggal < 32) {
                                                                    if ($d_now == $tanggal) {
                                                                        ?>
                                                                        <option selected value="<?php echo $tanggal; ?>"><?php echo $tanggal++; ?></option>
                                                                    <?php } else { ?>
                                                                        <option value="<?php echo $tanggal; ?>"><?php echo $tanggal++; ?></option>
                                                                    <?php
                                                                    }
                                                                }
                                                                ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <div class="input-group mb-2">
                                                            <div class="input-group-prepend">
                                                                <div class="input-group-text">M</div>
                                                            </div>
                                                            <select name="bulan" class="form-control custom-select">
                                                                <?php
                                                                $bulan = 01;
                                                                $m_now = date('m', strtotime($a['tanggal_lahir']));
                                                                while ($bulan < 13) {
                                                                    if ($m_now == $bulan) {
                                                                        ?>
                                                                        <option selected value="<?php echo $bulan; ?>"><?php echo $bulan++; ?></option>
                                                                    <?php } else { ?>
                                                                        <option value="<?php echo $bulan; ?>"><?php echo $bulan++; ?></option>
                                                                    <?php
                                                                    }
                                                                }
                                                                ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <div class="input-group mb-2">
                                                            <div class="input-group-prepend">
                                                                <div class="input-group-text">Y</div>
                                                            </div>
                                                            <select name="tahun" class="form-control custom-select">
                                                                <?php
                                                                $tahun = 1990;
                                                                $y_now = date('Y', strtotime($a['tanggal_lahir']));
                                                                while ($tahun < 2011) {
                                                                    if ($y_now == $tahun) {
                                                                        ?>
                                                                        <option selected value="<?php echo $tahun; ?>"><?php echo $tahun++; ?></option>
                                                                    <?php } else { ?>
                                                                        <option value="<?php echo $tahun; ?>"><?php echo $tahun++; ?></option>
                                                                    <?php
                                                                    }
                                                                }
                                                                ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label for="username">Nomor Telepon</label>
                                                    <input type="text" name="nomor_telepon" class="form-control" data-mask="0000-0000-0000" data-mask-clearifnotmatch="true" placeholder="0000-0000-0000" value="<?php echo $a['nomor_kontak']; ?>" />
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label for="email">Status Perkawinan</label>
                                                    <select name="status_perkawinan" class="form-control custom-select">
                                                        <?php $sp = array('Belum Menikah', 'Menikah', 'Janda/Duda'); ?>
                                                        <?php for ($x = 0; $x < count($sp); $x++) {
                                                            if ($a['status_perkawinan'] == $sp[$x]) {
                                                                ?>
                                                                <option selected value="<?php echo $sp[$x] ?>"><?php echo $sp[$x] ?></option>
    <?php } else { ?>
                                                                <option value="<?php echo $sp[$x] ?>"><?php echo $sp[$x] ?></option>
    <?php }
}
?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <label for="alamat">Alamat</label>
                                                    <textarea class="form-control" type="textarea" id="alamat" name="alamat" placeholder="Alamat" maxlength="140" rows="3"><?php echo $a['alamat']; ?></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="border-top">
                                        <div class="card-body text-right">
                                            <button type="reset" class="btn btn-warning">Reset</button>
                                            <button type="submit" class="btn btn-info">Update Profile</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div><!-- end col -->
                </div><!-- end row -->
            </div><!-- end container -->
        </div><!-- end section -->

        <footer class="footer">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4 col-md-4 col-xs-12">
                        <div class="widget clearfix">
                            <div class="widget-title">
                                <h3>Tentang</h3>
                            </div>
                            <p> Spritual,Kreativitas dan Intelektual menjadi kekuatan ESQ Business School dalam pembentukan karakter mahasiswanya.</p>
                            <div class="footer-right">
                                <ul class="footer-links-soi">
                                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                    <li><a href="#"><i class="fa fa-github"></i></a></li>
                                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                    <li><a href="#"><i class="fa fa-dribbble"></i></a></li>
                                    <li><a href="#"><i class="fa fa-pinterest"></i></a></li>
                                </ul>
                                <!-- end links -->
                            </div>
                        </div>
                        <!-- end clearfix -->
                    </div>
                    <!-- end col -->
                    <div class="col-lg-4 col-md-4 col-xs-12">
                        <div class="widget clearfix">
                            <div class="widget-title">
                                <h3>Link Informasi </h3>
                            </div>
                            <ul class="footer-links">
                                <li><a href="index.php">Home</a></li>
                                <li><a href="tentang.php">About</a></li>
                                <li><a href="forum.php">Forum</a></li>
                                <li><a href="blog.php">Blog</a></li>
                                <li><a href="dosen.php">Dosen</a></li>
                                <li><a href="gallery.php">Gallery</a></li>
                                <li><a href="kontak.php">Contact</a></li>
                            </ul>
                            <!-- end links -->
                        </div>
                        <!-- end clearfix -->
                    </div>
                    <!-- end col -->
                    <div class="col-lg-4 col-md-4 col-xs-12">
                        <div class="widget clearfix">
                            <div class="widget-title">
                                <h3>Detail Kontak</h3>
                            </div>
                            <ul class="footer-links">
                                <li><a href="mailto:#">alumni165@esqbs.com</a></li>
                                <li><a href="#">www.alumniesqbs.com</a></li>
                                <li>40 jln.TB.Simatupang,Cilandak Timur,Jakarta Selatan,Jakarta.</li>
                                <li>+61 3 8376 6284</li>
                            </ul><!-- end links -->
                        </div><!-- end clearfix -->
                    </div><!-- end col -->

                </div><!-- end row -->
            </div><!-- end container -->
        </footer><!-- end footer -->

        <div id="modalUpdateFoto" class="modal fade">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Ganti Foto Profile</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form class="" method="post" action="config/edit_foto_profile.php?id_alumni=<?php echo $id_alumni ?>" enctype="multipart/form-data">
                        <div class="modal-body">
                            <div class="form-group">
                                <label>Upload Foto</label>
                                <input type="file" name="photo_profile" id="photo_profile" />
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                            <button type="submit" class="btn btn-info">Update Foto</button>
                        </div>
                    </form>
                </div>
            </div><!-- Modals -->
        </div>

        <div class="copyrights">
            <div class="container">
                <div class="footer-distributed">
                    <div class="footer-center">                   
                        <p class="footer-company-name">All Rights Reserved. &copy; 2018 <a href="#">SmartEDU</a> Design By : <a href="https://html.design/">html design</a></p>
                    </div>
                </div>
            </div><!-- end container -->
        </div><!-- end copyrights -->

        <a href="#" id="scroll-to-top" class="dmtop global-radius"><i class="fa fa-angle-up"></i></a>

        <!-- ALL JS FILES -->
        <script src="js/all.js"></script>
        <!-- ALL PLUGINS -->
        <script src="js/custom.js"></script>
    </body>
</html>
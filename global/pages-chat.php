<?php
include 'config/koneksi.php';
session_start();
$_SESSION['url'] = "http://fumino-furuhashi.san/global/";
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- Basic -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">   

        <!-- Mobile Metas -->
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Site Metas -->
        <title>Alumni - ESQ Business School</title>
        <meta name="keywords" content="">
        <meta name="description" content="">
        <meta name="author" content="">

        <!-- Site Icons -->
        <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
        <link rel="apple-touch-icon" href="images/apple-touch-icon.png">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <!-- Site CSS -->
        <link rel="stylesheet" href="style.css">
        <!-- ALL VERSION CSS -->
        <link rel="stylesheet" href="css/versions.css">
        <!-- Responsive CSS -->
        <link rel="stylesheet" href="css/responsive.css">
        <!-- Custom CSS -->
        <link rel="stylesheet" href="css/custom.css">
        <link rel="stylesheet" href="css/chat.css"> 

        <!-- Modernizer for Portfolio -->
        <script src="js/modernizer.js"></script>

        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

    </head>
    <body class="host_version">

        <!-- LOADER -->
        <div id="preloader">
            <div class="loader-container">
                <div class="progress-br float shadow">
                    <div class="progress__item"></div>
                </div>
            </div>
        </div>
        <!-- END LOADER -->	

        <!-- Start header -->
        <header class="top-navbar">
            <nav class="navbar navbar-expand-lg navbar-light bg-light fixed-top">
                <div class="container-fluid">
                    <a class="navbar-brand" href="index.php">
                        <img src="images/6.png" alt="" />
                    </a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbars-host" aria-controls="navbars-rs-food" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbars-host">
                        <ul class="navbar-nav ml-auto">
                            <li class="nav-item"><a class="nav-link" href="index.php">Home</a></li>
                            <li class="nav-item"><a class="nav-link" href="tentang.php">Tentang</a></li>
                            <li class="nav-item"><a class="nav-link" href="forum.php">Forum</a></li>
                            <li class="nav-item"><a class="nav-link" href="blog.php">Blog</a></li>
                            <li class="nav-item"><a class="nav-link" href="dosen.php">Dosen</a></li>
                            <li class="nav-item"><a class="nav-link" href="gallery.php">Gallery</a></li>
                            <li class="nav-item"><a class="nav-link" href="kontak.php">Kontak</a></li>
                        </ul>
                        <ul class="nav navbar-nav navbar-right">
                            <?php if (!isset($_SESSION['id_alumni'])) { ?>
                                <li><a class="hover-btn-new log orange" href="authentication-login.php"><span>Login</span></a></li>
                            <?php } else { ?>
                                <li class="nav-item dropdown">
                                    <?php
                                    $id_alumni = $_SESSION['id_alumni'];
                                    $alumni = mysqli_query($con, "SELECT * FROM user WHERE id_user='$id_alumni'");
                                    $a = mysqli_fetch_array($alumni);
                                    ?>
                                    <a class="hover-btn-new log orange" href="authentication-login.php" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span><?php echo $a['nama']; ?></span></a>
                                    <div class="dropdown-menu dropdown-menu-right user-dd animated">
                                        <a class="dropdown-item" href="profile.php?id_alumni=<?php echo $id_alumni ?>">My Profile</a>
                                        <a class="dropdown-item" href="change_password.php?id_alumni=<?php echo $id_alumni ?>">Change Password</a>
                                        <div class="dropdown-divider"></div>
                                        <a class="dropdown-item" href="pages-chat.php">Message</a>
                                        <div class="dropdown-divider"></div>
                                        <a class="dropdown-item" href="config/logout.php">Logout</a>
                                    </div>
                                </li>
                            <?php } ?>
                        </ul>
                    </div>
                </div>
            </nav>
        </header>
        <!-- End header -->
        <br><br>
        <div class="all-title-box">
            <div class="container text-center">
                <h1>MESSAGE<span class="m_1">Alumni ESQ BUSINESS SCHOOL</span></h1>
            </div>
        </div>

        <div id="overviews" class="section wb">
            <div class="container-fluid">
                <div class="messaging">
                    <div class="inbox_msg">
                        <div class="inbox_people">
                            <div class="headind_srch">
                                <div class="recent_heading">
                                    <h4>Recent</h4>
                                </div>
                                <div class="srch_bar">
                                    <div class="stylish-input-group">
                                        <input type="text" class="search-bar"  placeholder="Search" >
                                        <span class="input-group-addon">
                                            <button type="button"> <i class="fa fa-search" aria-hidden="true"></i> </button>
                                        </span> </div>
                                </div>
                            </div>
                            <div class="inbox_chat">
                                <div class="chat_list active_chat">
                                    <div class="chat_people">
                                        <div class="chat_img"> <img src="https://ptetutorials.com/images/user-profile.png" alt="sunil"> </div>
                                        <div class="chat_ib">
                                            <h5>Sunil Rajput <span class="chat_date">Dec 25</span></h5>
                                            <p>Test, which is a new approach to have all solutions 
                                                astrology under one roof.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="mesgs">
                            <div class="msg_history">
                                <div class="outgoing_msg">
                                    <div class="sent_msg">
                                        <p>Apollo University, Delhi, India Test</p>
                                        <span class="time_date"> 11:01 AM    |    Today</span> </div>
                                </div>
                                <div class="incoming_msg">
                                    <div class="incoming_msg_img"> <img src="https://ptetutorials.com/images/user-profile.png" alt="sunil"> </div>
                                    <div class="received_msg">
                                        <div class="received_withd_msg">
                                            <p>We work directly with our designers and suppliers,
                                                and sell direct to you, which means quality, exclusive
                                                products, at a price anyone can afford.</p>
                                            <span class="time_date"> 11:01 AM    |    Today</span></div>
                                    </div>
                                </div>
                            </div>
                            <div class="type_msg">
                                <div class="input_msg_write">
                                    <input type="text" class="write_msg" placeholder="Type a message" />
                                    <button class="msg_send_btn" type="button"><i class="fa fa-paper-plane-o" aria-hidden="true"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- end container -->
        </div><!-- end section -->

        <footer class="footer">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4 col-md-4 col-xs-12">
                        <div class="widget clearfix">
                            <div class="widget-title">
                                <h3>Tentang</h3>
                            </div>
                            <p> Spritual,Kreativitas dan Intelektual menjadi kekuatan ESQ Business School dalam pembentukan karakter mahasiswanya.</p>
                            <div class="footer-right">
                                <ul class="footer-links-soi">
                                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                    <li><a href="#"><i class="fa fa-github"></i></a></li>
                                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                    <li><a href="#"><i class="fa fa-dribbble"></i></a></li>
                                    <li><a href="#"><i class="fa fa-pinterest"></i></a></li>
                                </ul>
                                <!-- end links -->
                            </div>
                        </div>
                        <!-- end clearfix -->
                    </div>
                    <!-- end col -->
                    <div class="col-lg-4 col-md-4 col-xs-12">
                        <div class="widget clearfix">
                            <div class="widget-title">
                                <h3>Link Informasi </h3>
                            </div>
                            <ul class="footer-links">
                                <li><a href="index.php">Home</a></li>
                                <li><a href="tentang.php">About</a></li>
                                <li><a href="forum.php">Forum</a></li>
                                <li><a href="blog.php">Blog</a></li>
                                <li><a href="dosen.php">Dosen</a></li>
                                <li><a href="gallery.php">Gallery</a></li>
                                <li><a href="kontak.php">Contact</a></li>
                            </ul>
                            <!-- end links -->
                        </div>
                        <!-- end clearfix -->
                    </div>
                    <!-- end col -->
                    <div class="col-lg-4 col-md-4 col-xs-12">
                        <div class="widget clearfix">
                            <div class="widget-title">
                                <h3>Detail Kontak</h3>
                            </div>
                            <ul class="footer-links">
                                <li><a href="mailto:#">alumni165@esqbs.com</a></li>
                                <li><a href="#">www.alumniesqbs.com</a></li>
                                <li>40 jln.TB.Simatupang,Cilandak Timur,Jakarta Selatan,Jakarta.</li>
                                <li>+61 3 8376 6284</li>
                            </ul><!-- end links -->
                        </div><!-- end clearfix -->
                    </div><!-- end col -->
                </div><!-- end row -->
            </div><!-- end container -->
        </footer><!-- end footer -->

        <div class="copyrights">
            <div class="container">
                <div class="footer-distributed">
                    <div class="footer-center">                   
                        <p class="footer-company-name">All Rights Reserved. &copy; 2018 <a href="#">SmartEDU</a> Design By : <a href="https://html.design/">html design</a></p>
                    </div>
                </div>
            </div><!-- end container -->
        </div><!-- end copyrights -->

        <a href="#" id="scroll-to-top" class="dmtop global-radius"><i class="fa fa-angle-up"></i></a>

        <!-- ALL JS FILES -->
        <script src="js/all.js"></script>
        <!-- ALL PLUGINS -->
        <script src="js/custom.js"></script>

    </body>
</html>
-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 29 Apr 2019 pada 19.40
-- Versi server: 10.1.35-MariaDB
-- Versi PHP: 7.2.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `alumnidb`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `blog`
--

CREATE TABLE `blog` (
  `id_blog` int(5) NOT NULL,
  `isi` text NOT NULL,
  `judul` varchar(100) NOT NULL,
  `photo` varchar(100) NOT NULL,
  `tanggal` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `blog`
--

INSERT INTO `blog` (`id_blog`, `isi`, `judul`, `photo`, `tanggal`) VALUES
(1, 'Bisnis mahasiswa EBS ini semakin berkembang dan baru baru ini membuka cabang ke 12 nya di indonesia. Yang membuatnya keren lagi barbeshop ini pernah didatangi oleh presiden RI kita, Bapak Joko widodo.', 'Hungky Dory Barbershop', '1.jpg', '2019-04-30 00:09:03'),
(2, 'Ya Rafins adalah bisnis terbaru dari mahasiswa baru Angkatan Ke-6 EBS. Kulit ikan yang dipadu dengan salted egg dan disajikan dalam bentuk snack yang gurih.', 'Rafin\'s Snack', '2.jpg', '2019-04-30 00:09:17');

-- --------------------------------------------------------

--
-- Struktur dari tabel `chat`
--

CREATE TABLE `chat` (
  `id` int(5) NOT NULL,
  `chat` text NOT NULL,
  `dari` varchar(20) NOT NULL,
  `ke` varchar(20) NOT NULL,
  `tanggal_waktu` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `chat`
--

INSERT INTO `chat` (`id`, `chat`, `dari`, `ke`, `tanggal_waktu`) VALUES
(1, 'Woy wan, gimana ngeditnya ? udah selesai', 'mutashimbillah7', 'iwansinanto', '2019-04-30 00:36:37'),
(2, 'Jangan lupa pull kalo udah', 'mutashimbillah7', 'iwansinanto', '2019-04-30 00:36:50'),
(3, 'udah nih gua edit blog sama data alumni', 'iwansinanto', 'mutashimbillah7', '2019-04-30 00:37:14'),
(4, 'Yaudah langsung aja di push, jangan lupa kasih tau di line apa aja yang di rubah', 'mutashimbillah7', 'iwansinanto', '2019-04-30 00:38:37'),
(5, 'ad, besok siap kan?', 'mutashimbillah7', 'fuadzikri', '2019-04-30 00:38:51'),
(6, 'udah redi semua lah mantap', 'fuadzikri', 'mutashimbillah7', '2019-04-30 00:39:08');

-- --------------------------------------------------------

--
-- Struktur dari tabel `chat_forum`
--

CREATE TABLE `chat_forum` (
  `id_chat_forum` int(5) NOT NULL,
  `dari` varchar(20) NOT NULL,
  `chat` text NOT NULL,
  `tanggal_waktu` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_forum` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `chat_forum`
--

INSERT INTO `chat_forum` (`id_chat_forum`, `dari`, `chat`, `tanggal_waktu`, `id_forum`) VALUES
(1, '1710130010', 'Woy bro gua mau sharing nih sedikit tentang cara memulai bisnis', '2019-04-30 00:12:11', 1),
(2, '1710130004', 'Gimana tuh caranya?', '2019-04-30 00:12:40', 1),
(3, '1710130004', 'Nih ada info beasiswa buat anak CS masuk!', '2019-04-30 00:12:59', 2),
(4, '1710130007', 'Boleh tuh, gua juga baru mau mulai nih', '2019-04-30 00:13:27', 1),
(5, '1710130007', 'Jurusan CS aja nih? bahasa ga bisa ?', '2019-04-30 00:13:47', 2),
(6, '1710130007', 'Ayo yang jago nulis masuk', '2019-04-30 00:13:58', 3);

-- --------------------------------------------------------

--
-- Struktur dari tabel `dosen`
--

CREATE TABLE `dosen` (
  `id_dosen` int(5) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `program_studi` varchar(50) NOT NULL,
  `jabatan` varchar(50) NOT NULL,
  `photo` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `dosen`
--

INSERT INTO `dosen` (`id_dosen`, `nama`, `program_studi`, `jabatan`, `photo`) VALUES
(1, 'Bunda Lely', 'Management Business', 'Kepala Program Studi Business Management', 'LELY.jpg'),
(2, 'Bu Asri', 'Information System', 'Kepala Program Studi Sistem Informasi', 'ASRI.jpg'),
(3, 'Bu Ahlijati', 'Computer Science', 'Kepala Program Studi Ilmu Komputer', 'JATI.jpg');

-- --------------------------------------------------------

--
-- Struktur dari tabel `forum`
--

CREATE TABLE `forum` (
  `id_forum` int(5) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `photo` varchar(100) NOT NULL,
  `tanggal` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` varchar(10) NOT NULL,
  `id_kategori_forum` int(5) NOT NULL,
  `pembuat` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `forum`
--

INSERT INTO `forum` (`id_forum`, `nama`, `photo`, `tanggal`, `status`, `id_kategori_forum`, `pembuat`) VALUES
(1, 'Memulai Bisnis Untuk Para Pemula', '1.jpg', '2019-04-30 00:06:07', 'active', 2, '1710130010'),
(2, 'Beasiswa S2 Computer Science Tokyo Institute of Te', '2.jpg', '2019-04-30 00:06:50', 'active', 1, '1710130004'),
(3, 'Lomba Karya Tulis Ilmiah Nasional (LKTIN) 2019', '3.jpg', '2019-04-30 00:07:33', 'active', 3, '1710130007');

-- --------------------------------------------------------

--
-- Struktur dari tabel `gallery`
--

CREATE TABLE `gallery` (
  `id_gallery` int(5) NOT NULL,
  `photo` varchar(100) NOT NULL,
  `judul` varchar(50) NOT NULL,
  `keterangan` varchar(50) NOT NULL,
  `tanggal` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `gallery`
--

INSERT INTO `gallery` (`id_gallery`, `photo`, `judul`, `keterangan`, `tanggal`, `status`) VALUES
(1, '1.JPG', 'Angaktan 1', 'Wisuda Tahun 2017', '2019-04-30 00:16:10', 'Active'),
(2, '2.JPG', 'Foto Bersama Wali', 'Alumni 2017', '2019-04-30 00:16:58', 'Active'),
(3, '3.JPG', 'Prosesi Wisuda', 'Alumni 2017', '2019-04-30 00:17:18', 'Active'),
(4, '4.JPG', 'Wisudawan Masuk ke Ruangan', 'Alumni 2017', '2019-04-30 00:17:48', 'Active'),
(5, '5.JPG', 'Panggung Wisuda', 'Alumni 2017', '2019-04-30 00:18:00', 'Active'),
(6, '6.JPG', 'Penghargaan Mahasiswa Terbaik', 'Alumni 2017', '2019-04-30 00:18:37', 'Active'),
(7, '7.JPG', 'Alumni Berprestasi', 'Alumni 2017', '2019-04-30 00:18:52', 'Active'),
(8, '8.JPG', 'Alumni Business Management', 'Alumni 2017', '2019-04-30 00:19:06', 'Active');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kategori_forum`
--

CREATE TABLE `kategori_forum` (
  `id_kategori_forum` int(5) NOT NULL,
  `kategori_forum` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kategori_forum`
--

INSERT INTO `kategori_forum` (`id_kategori_forum`, `kategori_forum`) VALUES
(1, 'Beasiswa'),
(2, 'Pekerjaan'),
(3, 'Perlombaan');

-- --------------------------------------------------------

--
-- Struktur dari tabel `questionnaire_diri`
--

CREATE TABLE `questionnaire_diri` (
  `id_questionnaire_diri` int(11) NOT NULL,
  `nama_perusahaan` varchar(100) NOT NULL,
  `nama_leader` varchar(50) NOT NULL,
  `jabatan` varchar(50) NOT NULL,
  `kontak_kantor` varchar(15) NOT NULL,
  `email_kantor` varchar(50) NOT NULL,
  `alamat_kantor` text NOT NULL,
  `id_user` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `questionnaire_kegiatan`
--

CREATE TABLE `questionnaire_kegiatan` (
  `id_questionnaire_kegiatan` int(5) NOT NULL,
  `aktivitas_kemahasiswaan` text NOT NULL,
  `id_user` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `questionnaire_pekerjaan`
--

CREATE TABLE `questionnaire_pekerjaan` (
  `id_questionnaire_pekerjaan` int(5) NOT NULL,
  `id_user` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `questionnaire_studi`
--

CREATE TABLE `questionnaire_studi` (
  `id_questionnaire_studi` int(5) NOT NULL,
  `nama_sma` varchar(100) NOT NULL,
  `tahun_masuk_sma` year(4) NOT NULL,
  `tahun_lulus_sma` year(4) NOT NULL,
  `jurusan_sma` varchar(30) NOT NULL,
  `nama_s1` varchar(100) NOT NULL,
  `tahun_masuk_s1` year(4) NOT NULL,
  `tahun_lulus_s1` year(4) NOT NULL,
  `program_studi` varchar(50) NOT NULL,
  `ipk` varchar(1) NOT NULL,
  `judul_skripsi` varchar(100) NOT NULL,
  `pengalaman_akademik` text NOT NULL,
  `pendidikan_tambahan_selama` text NOT NULL,
  `pendidikan_tambahan_setelah` text NOT NULL,
  `id_user` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE `user` (
  `id_user` varchar(20) NOT NULL,
  `password` varchar(20) NOT NULL,
  `nama` varchar(20) NOT NULL,
  `tempat_lahir` varchar(20) NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `jenis_kelamin` varchar(10) NOT NULL,
  `status_perkawinan` varchar(15) NOT NULL,
  `nomor_kontak` varchar(15) NOT NULL,
  `email` varchar(50) NOT NULL,
  `alamat` text NOT NULL,
  `photo_profile` varchar(100) NOT NULL,
  `isi_questionnaire` tinyint(1) NOT NULL,
  `level` varchar(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`id_user`, `password`, `nama`, `tempat_lahir`, `tanggal_lahir`, `jenis_kelamin`, `status_perkawinan`, `nomor_kontak`, `email`, `alamat`, `photo_profile`, `isi_questionnaire`, `level`) VALUES
('1710130004', 'esqbs165', 'Iwan Sinanto Ate', '', '0000-00-00', '', '', '', '', '', 'default.jpg', 0, 'alumni'),
('1710130007', 'esqbs165', 'Muhammad Fuad Zikri', '', '0000-00-00', '', '', '', '', '', 'default.jpg', 0, 'alumni'),
('1710130010', 'dsasdaqwa', 'Mu\'tashim Billah', '', '0000-00-00', '', '', '', '', '', '1710130010.jpg', 0, 'alumni'),
('fuadzikri', 'fuadzikri', 'Muhammad Fu\'ad Zikri', 'Jakarta', '2019-04-01', 'Laki-laki', 'Belum Menikah', '081234567890', 'm.fuad.z@students.esqbs.ac.id', 'Jeruk Purut', 'fuadzikri.jpg', 1, 'admin'),
('iwansinanto', 'iwansinanto', 'Iwan Sinanto Ate', 'Aceh', '2019-04-01', 'Laki-laki', 'Belum Menikah', '081234567890', 'i.sinanto.a@students.esqbs.ac.', 'Jeruk Purut', 'iwansinanto.jpg', 1, 'admin'),
('mutashimbillah7', 'dsasdaqwa', 'Mu\'tashim Billah', 'Bekasi', '1998-11-07', 'Laki-laki', 'Belum Menikah', '081234567890', 'm.billah@students.esqbs.ac.id', 'Kp. Karang Congok RT.001/001 No. 41, Karang Satria', 'mutashimb7.png', 1, 'admin');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `blog`
--
ALTER TABLE `blog`
  ADD PRIMARY KEY (`id_blog`);

--
-- Indeks untuk tabel `chat`
--
ALTER TABLE `chat`
  ADD PRIMARY KEY (`id`),
  ADD KEY `dari` (`dari`),
  ADD KEY `ke` (`ke`);

--
-- Indeks untuk tabel `chat_forum`
--
ALTER TABLE `chat_forum`
  ADD PRIMARY KEY (`id_chat_forum`),
  ADD KEY `id_forum` (`id_forum`),
  ADD KEY `dari` (`dari`);

--
-- Indeks untuk tabel `dosen`
--
ALTER TABLE `dosen`
  ADD PRIMARY KEY (`id_dosen`);

--
-- Indeks untuk tabel `forum`
--
ALTER TABLE `forum`
  ADD PRIMARY KEY (`id_forum`),
  ADD KEY `id_kategori_forum` (`id_kategori_forum`),
  ADD KEY `pembuat` (`pembuat`);

--
-- Indeks untuk tabel `gallery`
--
ALTER TABLE `gallery`
  ADD PRIMARY KEY (`id_gallery`);

--
-- Indeks untuk tabel `kategori_forum`
--
ALTER TABLE `kategori_forum`
  ADD PRIMARY KEY (`id_kategori_forum`);

--
-- Indeks untuk tabel `questionnaire_diri`
--
ALTER TABLE `questionnaire_diri`
  ADD PRIMARY KEY (`id_questionnaire_diri`),
  ADD KEY `id_user` (`id_user`);

--
-- Indeks untuk tabel `questionnaire_kegiatan`
--
ALTER TABLE `questionnaire_kegiatan`
  ADD PRIMARY KEY (`id_questionnaire_kegiatan`),
  ADD KEY `id_user` (`id_user`);

--
-- Indeks untuk tabel `questionnaire_pekerjaan`
--
ALTER TABLE `questionnaire_pekerjaan`
  ADD PRIMARY KEY (`id_questionnaire_pekerjaan`),
  ADD KEY `id_user` (`id_user`);

--
-- Indeks untuk tabel `questionnaire_studi`
--
ALTER TABLE `questionnaire_studi`
  ADD PRIMARY KEY (`id_questionnaire_studi`),
  ADD KEY `id_user` (`id_user`);

--
-- Indeks untuk tabel `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `blog`
--
ALTER TABLE `blog`
  MODIFY `id_blog` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `chat`
--
ALTER TABLE `chat`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `chat_forum`
--
ALTER TABLE `chat_forum`
  MODIFY `id_chat_forum` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `dosen`
--
ALTER TABLE `dosen`
  MODIFY `id_dosen` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `forum`
--
ALTER TABLE `forum`
  MODIFY `id_forum` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `gallery`
--
ALTER TABLE `gallery`
  MODIFY `id_gallery` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT untuk tabel `kategori_forum`
--
ALTER TABLE `kategori_forum`
  MODIFY `id_kategori_forum` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `questionnaire_diri`
--
ALTER TABLE `questionnaire_diri`
  MODIFY `id_questionnaire_diri` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `questionnaire_kegiatan`
--
ALTER TABLE `questionnaire_kegiatan`
  MODIFY `id_questionnaire_kegiatan` int(5) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `questionnaire_pekerjaan`
--
ALTER TABLE `questionnaire_pekerjaan`
  MODIFY `id_questionnaire_pekerjaan` int(5) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `questionnaire_studi`
--
ALTER TABLE `questionnaire_studi`
  MODIFY `id_questionnaire_studi` int(5) NOT NULL AUTO_INCREMENT;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `chat`
--
ALTER TABLE `chat`
  ADD CONSTRAINT `chat_ibfk_1` FOREIGN KEY (`dari`) REFERENCES `user` (`id_user`),
  ADD CONSTRAINT `chat_ibfk_2` FOREIGN KEY (`ke`) REFERENCES `user` (`id_user`);

--
-- Ketidakleluasaan untuk tabel `chat_forum`
--
ALTER TABLE `chat_forum`
  ADD CONSTRAINT `chat_forum_ibfk_2` FOREIGN KEY (`id_forum`) REFERENCES `forum` (`id_forum`),
  ADD CONSTRAINT `chat_forum_ibfk_3` FOREIGN KEY (`dari`) REFERENCES `user` (`id_user`);

--
-- Ketidakleluasaan untuk tabel `forum`
--
ALTER TABLE `forum`
  ADD CONSTRAINT `forum_ibfk_1` FOREIGN KEY (`id_kategori_forum`) REFERENCES `kategori_forum` (`id_kategori_forum`),
  ADD CONSTRAINT `forum_ibfk_2` FOREIGN KEY (`pembuat`) REFERENCES `user` (`id_user`);

--
-- Ketidakleluasaan untuk tabel `questionnaire_diri`
--
ALTER TABLE `questionnaire_diri`
  ADD CONSTRAINT `questionnaire_diri_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`);

--
-- Ketidakleluasaan untuk tabel `questionnaire_kegiatan`
--
ALTER TABLE `questionnaire_kegiatan`
  ADD CONSTRAINT `questionnaire_kegiatan_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`);

--
-- Ketidakleluasaan untuk tabel `questionnaire_pekerjaan`
--
ALTER TABLE `questionnaire_pekerjaan`
  ADD CONSTRAINT `questionnaire_pekerjaan_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`);

--
-- Ketidakleluasaan untuk tabel `questionnaire_studi`
--
ALTER TABLE `questionnaire_studi`
  ADD CONSTRAINT `questionnaire_studi_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

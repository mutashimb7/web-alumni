<?php
include "config/koneksi.php";
session_start();
if (!isset($_SESSION['username'])) {
    header("location:authentication-login.php");
}
?>
<!DOCTYPE html>
<html dir="ltr" lang="en">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Tell the browser to be responsive to screen width -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="stylesheet" href="vendor/css/style.css" />
        <!-- Favicon icon -->
        <link rel="icon" type="image/png" sizes="16x16" href="assets/images/favicon.png">
        <title>Alumni ESQBS</title>
        <!-- Custom CSS -->
        <link rel="stylesheet" type="text/css" href="assets/extra-libs/multicheck/multicheck.css">
        <link href="assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.css" rel="stylesheet">
        <link href="vendor/css/style.min.css" rel="stylesheet">
    </head>
    <?php
    $data = mysqli_query($con, "SELECT * FROM user");
    ?>

    <body>
        <div class="preloader">
            <div class="lds-ripple">
                <div class="lds-pos"></div>
                <div class="lds-pos"></div>
            </div>
        </div>
        <div id="main-wrapper">
            <header class="topbar" data-navbarbg="skin5">
                <nav class="navbar top-navbar navbar-expand-md navbar-dark">
                    <div class="navbar-header" data-logobg="skin5">
                        <!-- This is for the sidebar toggle which is visible on mobile only -->
                        <a class="nav-toggler waves-effect waves-light d-block d-md-none" href="javascript:void(0)"><i class="ti-menu ti-close"></i></a>
                        <!-- Logo icon -->
                        <b class="logo-icon p-l-10">
                        <!--You can put here icon as well // <i class="wi wi-sunset"></i> //-->
                            <!-- Dark Logo icon -->
                            <!--img src="img/logo/grad2.png" alt="homepage" class="light-logo" /-->
                        </b>
                        <!--End Logo icon -->
                        <!-- Logo text -->
                        <span class="logo-text">
                            <!-- dark Logo text -->
                        </span>
                        </a>
                        <a class="topbartoggler d-block d-md-none waves-effect waves-light" href="javascript:void(0)" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"><i class="ti-more"></i></a>
                    </div>
                    <div class="navbar-collapse collapse" id="navbarSupportedContent" data-navbarbg="skin5">
                        <ul class="navbar-nav float-left mr-auto">
                            <li class="nav-item search-box"> <a class="nav-link waves-effect waves-dark" href="javascript:void(0)"><i class="ti-search"></i></a>
                                <form class="app-search position-absolute">
                                    <input type="text" class="form-control" placeholder="Search &amp; enter"> <a class="srh-btn"><i class="ti-close"></i></a>
                                </form>
                            </li>
                        </ul>
                        <ul class="navbar-nav float-right">
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle waves-effect waves-dark" href="" id="2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="mdi mdi-bell font-24"></i>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right mailbox animated bounceInDown" aria-labelledby="2">
                                    <ul class="list-style-none">
                                        <li>
                                            <div class="">
                                                <!-- Message -->
                                                <a href="javascript:void(0)" class="link border-top">
                                                    <div class="d-flex no-block align-items-center p-10">
                                                        <span class="btn btn-success btn-circle"><i class="ti-calendar"></i></span>
                                                        <div class="m-l-10">
                                                            <h5 class="m-b-0">Acara</h5>
                                                            <span class="mail-desc">ngingetin tentang acara</span>
                                                        </div>
                                                    </div>
                                                </a>
                                                <!-- Message -->
                                                <a href="javascript:void(0)" class="link border-top">
                                                    <div class="d-flex no-block align-items-center p-10">
                                                        <span class="btn btn-info btn-circle"><i class="ti-settings"></i></span>
                                                        <div class="m-l-10">
                                                            <h5 class="m-b-0">Pengaturan</h5>
                                                            <span class="mail-desc">biasalah ngatur</span>
                                                        </div>
                                                    </div>
                                                </a>
                                                <!-- Message -->
                                                <a href="javascript:void(0)" class="link border-top">
                                                    <div class="d-flex no-block align-items-center p-10">
                                                        <span class="btn btn-primary btn-circle"><i class="ti-user"></i></span>
                                                        <div class="m-l-10">
                                                            <h5 class="m-b-0">Admin Ganteng</h5>
                                                            <span class="mail-desc">kepoin admin</span>
                                                        </div>
                                                    </div>
                                                </a>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            <li class="nav-item dropdown">
                                <?php
                                $username = $_SESSION['username'];
                                $p = mysqli_fetch_array(mysqli_query($con, "SELECT * FROM user WHERE id_user='$username'"));
                                ?>
                                <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark pro-pic" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="<?php echo 'img/photo_profile/' . $p['photo_profile']; ?>" alt="user" class="rounded-circle" width="31"></a>
                                <div class="dropdown-menu dropdown-menu-right user-dd animated">
                                    <a class="dropdown-item" href="profile.php?username=<?php echo $username ?>"><i class="ti-user m-r-5 m-l-5"></i> My Profile</a>
                                    <a class="dropdown-item" href="change_password.php?username=<?php echo $username ?>"><i class="ti-settings m-r-5 m-l-5"></i> Change Password</a>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="authentication-login.php"><i class="fa fa-power-off m-r-5 m-l-5"></i> Logout</a>
                                </div>
                            </li>
                        </ul>
                    </div>
                </nav>
            </header>
            <aside class="left-sidebar" data-sidebarbg="skin5">
                <!-- Sidebar scroll-->
                <div class="scroll-sidezbar">
                    <!-- Sidebar navigation-->
                    <nav class="sidebar-nav">
                        <ul id="sidebarnav" class="p-t-30">
                            <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="index.php" aria-expanded="false"><i class="mdi mdi-home"></i><span class="hide-menu">Home</span></a></li>
                            <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="data_alumni.php" aria-expanded="false"><i class="mdi mdi-account-card-details"></i><span class="hide-menu">Data Alumni</span></a></li>
                            <li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-glassdoor"></i><span class="hide-menu">Room</span></a>
                                <ul aria-expanded="false" class="collapse  first-level">
                                    <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="forum_forum.php" aria-expanded="false"><i class="fas fa-people-carry"></i><span class="hide-menu">Forum-forum</span></a></li>
                                    <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="blog.php" aria-expanded="false"><i class="fas fa-newspaper"></i><span class="hide-menu">Blog</span></a></li>
                                </ul>
                            </li>

                            <li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-animation"></i><span class="hide-menu">Media</span></a>
                                <ul aria-expanded="false" class="collapse  first-level">
                                    <li class="sidebar-item"><a href="pages-gallery.php" class="sidebar-link"><i class="fas fa-film"></i><span class="hide-menu"> Gallery </span></a></li>
                                    <li class="sidebar-item"><a href="pages-chat.php" class="sidebar-link"><i class="mdi mdi-message-outline"></i><span class="hide-menu"> Chat Option </span></a></li>
                                </ul>
                            </li>
                        </ul>
                    </nav>
                </div>
            </aside>
            <div class="page-wrapper">
                <div class="page-breadcrumb">
                    <div class="row">
                        <div class="col-12 d-flex no-block align-items-center">
                            <h4 class="page-title">Data Alumni</h4>
                            <div class="ml-auto text-right">
                                <nav aria-label="breadcrumb">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                                        <li class="breadcrumb-item active" aria-current="page">Library</li>
                                    </ol>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="modalAdd" class="modal fade">
                    <div class="modal-dialog">
                        <form method="post" action="config/save_data_alumni.php" role="form">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Data-Data Alumni</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <div class="form-group">
                                        <b><label for="id_alumni">ID Alumni</label></b>
                                        <input type="text" class="form-control" id="id_user" name="id_alumni" placeholder="ID Alumni" required>
                                    </div>
                                    <div class="form-group">
                                        <b><label for="nama">Nama Lengkap</label></b>
                                        <input type="text" class="form-control" id="nama" name="nama" placeholder="Nama Lengkap" required>
                                    </div>
                                    <div class="form-group">
                                        <b><label for="email">Email</label></b>
                                        <input type="text" class="form-control" id="email" name="email" placeholder="Email" required>
                                    </div>
                                </div>
                                <div class="card-footer text-right">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                    <button type="submit" id="submit" name="submit" class="btn btn-info ml-2">Add</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-body">
                                    <div class="text-right">
                                        <button type="button" class="btn btn-lg btn-success" data-toggle="modal" data-target="#modalAdd"><i class="fas fa-user-plus"></i></button>
                                        </br>
                                        </br>
                                    </div>
                                    <div class="table-responsive">
                                        <table id="zero_config" class="table table-striped table-bordered">
                                            <thead>
                                                <tr class="text-center">
                                                    <th>Nama Lengkap</th>
                                                    <th>Tempat Lahir</th>
                                                    <th>Tanggal Lahir</th>
                                                    <th>Jenis Kelamin</th>
                                                    <th>Nomor Kontak</th>
                                                    <th>Email</th>
                                                    <th>Alamat</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                while ($g = mysqli_fetch_array($data)) {
                                                    if ($g['level'] == "alumni") {
                                                        ?>
                                                        <tr>
                                                            <td>
                                                                <?php echo $g['nama'] ?>
                                                            </td>
                                                            <td>
                                                                <?php echo $g['tempat_lahir'] ?>
                                                            </td>
                                                            <td>
                                                                <?php
                                                                $tgl = $g['tanggal_lahir'];
                                                                echo date("d-m-Y", strtotime($tgl));
                                                                ?>
                                                            </td>
                                                            <td>
                                                                <?php echo $g['jenis_kelamin'] ?>
                                                            </td>
                                                            <td>
                                                                <?php echo $g['nomor_kontak'] ?>
                                                            </td>
                                                            <td>
                                                                <?php echo $g['email'] ?>
                                                            </td>
                                                            <td>
        <?php echo $g['alamat'] ?>
                                                            </td>
                                                            <td>
                                                                <button type="button" class="btn btn-xs btn-warning" data-toggle="modal" data-target="#edit-<?php echo $g['id_user'] ?>"><span class="ti-pencil-alt"></span></button>
                                                                <button type="button" class="btn btn-xs btn-danger" data-toggle="modal" data-target="#delete-<?php echo $g['id_user'] ?>"><span class="ti-trash"></span></button>
                                                                <button type="button" class="btn btn-xs btn-info" data-toggle="modal" data-target="#info-<?php echo $g['id_user'] ?>"><span class="ti-info-alt"></span></button>
                                                            </td>
                                                        </tr>
                                                        <!-- Trigger the modal with a button -->
                                                        <!-- Modal -->
                                                    <div id="edit-<?php echo $g['id_user']; ?>" class="modal" role="dialog">
                                                        <div class="modal-dialog">
                                                            <!-- Modal content-->
                                                            <div class="modal-content">
                                                                <!-- Modal Header -->
                                                                <div class="modal-header">
                                                                    <h4 class="modal-title">Edit Data Alumni</h4>
                                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                                </div>
                                                                <form method="post" action="config/edit_data_alumni.php?id_alumni= <?php echo $g['id_user'] ?>" role="form">
                                                                    <div class="modal-body">
                                                                        <div class="row">
                                                                            <div class="col-md-12">
                                                                                <div class="form-group">
                                                                                    <label>Nama Lengkap</label>
                                                                                    <input type="text" class="form-control" id="nama" name="nama" placeholder="" value="<?php echo $g['nama'] ?>" required>
                                                                                </div>
                                                                                <div class="form-group">
                                                                                    <b><label for="tempat_lahir">Tempat Lahir</label></b>
                                                                                    <input type="text" class="form-control" id="tempat_lahir" name="tempat_lahir" placeholder="" value="<?php echo $g['tempat_lahir'] ?>" required>
                                                                                </div>
                                                                                <div class="form-group">
                                                                                    <b><label for="tanggalLahir">Tanggal Lahir</label></b>
                                                                                    <div class="row">
                                                                                        <div class="col-sm-4">
                                                                                            <select name="tanggal" class="form-control custom-select">
                                                                                                <?php
                                                                                                $tanggal = 01;
                                                                                                $d_now = date('d', strtotime($g['tanggal_lahir']));
                                                                                                while ($tanggal < 32) {
                                                                                                    if ($d_now == $tanggal) {
                                                                                                        ?>
                                                                                                        <option selected value="<?php echo $tanggal; ?>"><?php echo $tanggal++; ?></option>
                                                                                                    <?php } else { ?>
                                                                                                        <option value="<?php echo $tanggal; ?>"><?php echo $tanggal++; ?></option>
                                                                                                    <?php
                                                                                                    }
                                                                                                }
                                                                                                ?>
                                                                                            </select>
                                                                                        </div>
                                                                                        <div class="col-sm-4">
                                                                                            <select name="bulan" class="form-control custom-select">
                                                                                                <?php
                                                                                                $bulan = 01;
                                                                                                $m_now = date('m', strtotime($g['tanggal_lahir']));
                                                                                                while ($bulan < 13) {
                                                                                                    if ($m_now == $bulan) {
                                                                                                        ?>
                                                                                                        <option selected value="<?php echo $bulan; ?>"><?php echo $bulan++; ?></option>
                                                                                                    <?php } else { ?>
                                                                                                        <option value="<?php echo $bulan; ?>"><?php echo $bulan++; ?></option>
            <?php
            }
        }
        ?>
                                                                                            </select>
                                                                                        </div>
                                                                                        <div class="col-sm-4">
                                                                                            <select name="tahun" class="form-control custom-select">
                                                                                                <?php
                                                                                                $tahun = 1990;
                                                                                                $y_now = date('Y', strtotime($g['tanggal_lahir']));
                                                                                                while ($tahun < 2011) {
                                                                                                    if ($y_now == $tahun) {
                                                                                                        ?>
                                                                                                        <option selected value="<?php echo $tahun; ?>"><?php echo $tahun++; ?></option>
                                                                                                    <?php } else { ?>
                                                                                                        <option value="<?php echo $tahun; ?>"><?php echo $tahun++; ?></option>
            <?php
            }
        }
        ?>
                                                                                            </select>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="form-group">
                                                                                    <b><label for="jenis_kelamin">Jenis Kelamin</label></b>
                                                                                    <div class="row">
        <?php
        $jenis_kelamin = $g['jenis_kelamin'];
        if ($jenis_kelamin == "Laki-laki") {
            ?>
                                                                                            <div class="col-sm-3">
                                                                                                <input type="radio" name="jenis_kelamin" value="Laki-laki" checked> Laki-Laki
                                                                                            </div>
                                                                                            <div class="col-sm-3">
                                                                                                <input type="radio" name="jenis_kelamin" value="Perempuan"> Perempuan
                                                                                            </div>
        <?php } else { ?>
                                                                                            <div class="col-sm-3">
                                                                                                <input type="radio" name="jenis_kelamin" value="Perempuan" checked> Perempuan
                                                                                            </div>
                                                                                            <div class="col-sm-3">
                                                                                                <input type="radio" name="jenis_kelamin" value="Laki-laki"> Laki-Laki
                                                                                            </div>
                                                                                        <?php } ?>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="form-group">
                                                                                    <b><label for="status_perkawinan">Status Perkawinan</label></b>
                                                                                    <div class="row">
        <?php
        $status_perkawinan = $g['status_perkawinan'];
        if ($status_perkawinan == "Belum Menikah") {
            ?>
                                                                                            <div class="col-sm-4">
                                                                                                <input type="radio" name="status_perkawinan" value="Belum Menikah" checked> Belum Menikah
                                                                                            </div>
                                                                                            <div class="col-sm-4">
                                                                                                <input type="radio" name="status_perkawinan" value="Menikah"> Menikah
                                                                                            </div>
                                                                                            <div class="col-sm-4">
                                                                                                <input type="radio" name="status_perkawinan" value="Janda/Duda"> Janda/Duda
                                                                                            </div>
        <?php } else if ($status_perkawinan == "Menikah") { ?>
                                                                                            <div class="col-sm-4">
                                                                                                <input type="radio" name="status_perkawinan" value="Belum Menikah"> Belum Menikah
                                                                                            </div>
                                                                                            <div class="col-sm-4">
                                                                                                <input type="radio" name="status_perkawinan" value="Menikah" checked> Menikah
                                                                                            </div>
                                                                                            <div class="col-sm-4">
                                                                                                <input type="radio" name="status_perkawinan" value="Janda/Duda"> Janda/Duda
                                                                                            </div>
        <?php } else { ?>
                                                                                            <div class="col-sm-4">
                                                                                                <input type="radio" name="status_perkawinan" value="Belum Menikah"> Belum Menikah
                                                                                            </div>
                                                                                            <div class="col-sm-4">
                                                                                                <input type="radio" name="status_perkawinan" value="Menikah"> Menikah
                                                                                            </div>
                                                                                            <div class="col-sm-4">
                                                                                                <input type="radio" name="status_perkawinan" value="Janda/Duda" checked> Janda/Duda
                                                                                            </div>
        <?php } ?>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="form-group">
                                                                                    <b><label for="nomor_kontak">Nomor Kontak</label></b>
                                                                                    <input type="text" class="form-control" id="nomor_kontak" name="nomor_kontak" placeholder="08xxx" value="<?php echo $g['nomor_kontak'] ?>">
                                                                                </div>
                                                                                <div class="form-group">
                                                                                    <b><label for="email">Email</label></b>
                                                                                    <input type="text" class="form-control" id="email" name="email" placeholder="Email" value="<?php echo $g['email'] ?>">
                                                                                </div>
                                                                                <div class="form-group">
                                                                                    <b><label for="alamat">Alamat</label></b>
                                                                                    <textarea class="form-control" type="textarea" id="alamat" name="alamat" placeholder="Alamat" maxlength="140" rows="3"> <?php echo $g['alamat'] ?> </textarea>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="modal-footer">
                                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                                                        <button type="submit" id="submit" name="submit" class="btn btn-info ml-2">Update</button>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div id="delete-<?php echo $g['id_user'] ?>" class="modal fade">
                                                        <div class="modal-dialog">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <h5 class="modal-title">Hapus Data</h5>
                                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                        <span aria-hidden="true">&times;</span>
                                                                    </button>
                                                                </div>
                                                                <div class="modal-body">
                                                                    <div class="form-group">
                                                                        <label>Apakah anda yakin ingin menghapus data ini?</label>
                                                                    </div>
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <div class="col-sm-6 text-right">
                                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                                                        <a href="config/delete_alumni.php?id_alumni=<?php echo $g['id_user']; ?>"><button type="submit" class="btn btn-danger">Delete</button></a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- Modals -->
                                                    </div>
    <?php }
}
?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                </br>
                </br>
                </br>
                <footer class="footer text-center">
                    Project Web Alumni ESQ Business School. Designed and Developed by <b>Computer Science I</b>
                </footer>
            </div>
            <div class="card-footer text-right fixed-bottom">
                <div class="btn-group dropup">
                    <button type="button" class="btn btn-lg btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fas fa-comment-alt"></i> Chat with Friends
                    </button>
                    <div class="dropdown-menu">
                        <?php
                        $all = mysqli_query($con, "SELECT * FROM user ORDER BY nama");
                        while ($admin = mysqli_fetch_array($all)) {
                            if (($username != $admin['id_user']) && $admin['level'] == "admin") {
                                ?>
                                <a class="dropdown-item" href="pages-chat.php?friends=<?php echo $admin['id_user']; ?>"><i class="fas fa-user"></i> <?php echo $admin['nama']; ?></a>
    <?php }
}
?>
                    </div>
                </div>
            </div>
        </div>

        <script src="assets/libs/jquery/dist/jquery.min.js"></script>
        <!-- Bootstrap tether Core JavaScript -->
        <script src="assets/libs/popper.js/dist/umd/popper.min.js"></script>
        <script src="assets/libs/bootstrap/dist/js/bootstrap.min.js"></script>
        <!-- slimscrollbar scrollbar JavaScript -->
        <script src="assets/libs/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js"></script>
        <script src="assets/extra-libs/sparkline/sparkline.js"></script>
        <!--Wave Effects -->
        <script src="vendor/js/waves.js"></script>
        <!--Menu sidebar -->
        <script src="vendor/js/sidebarmenu.js"></script>
        <!--Custom JavaScript -->
        <script src="vendor/js/custom.min.js"></script>
        <!-- this page js -->
        <script src="assets/extra-libs/multicheck/datatable-checkbox-init.js"></script>
        <script src="assets/extra-libs/multicheck/jquery.multicheck.js"></script>
        <script src="assets/extra-libs/DataTables/datatables.min.js"></script>
        <script>
            /****************************************
             *       Basic Table                   *
             ****************************************/
            $('#zero_config').DataTable();
        </script>
    </body>

</html>
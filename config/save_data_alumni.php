<?php

include "koneksi.php";

$permitted_chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

function generate_string($input, $strength = 16) {
    $input_length = strlen($input);
    $random_string = '';
    for ($i = 0; $i < $strength; $i++) {
        $random_character = $input[mt_rand(0, $input_length - 1)];
        $random_string .= $random_character;
    }

    return $random_string;
}

$password = "esqbs" . generate_string($permitted_chars, 5);
$nama = addslashes($_POST['nama']);
$photo_profile = "default.jpg";
$level = "alumni";

mysqli_query($con, "INSERT INTO user(id_user, password, nama, email, photo_profile, isi_questionnaire, level) VALUE('$_POST[id_alumni]', '$password', '$nama', '$_POST[email]', '$photo_profile', '0', '$level')");

header('location:../data_alumni.php');
?>
<?php
session_start();
include 'config/koneksi.php';
if (!isset($_SESSION['username'])) {
    header("location:authentication-login.php");
}
?>
<!DOCTYPE html>
<html dir="ltr" lang="en">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Tell the browser to be responsive to screen width -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="stylesheet" href="vendor/css/style.css" />
        <!-- Favicon icon -->
        <link rel="icon" type="image/png" sizes="16x16" href="assets/images/favicon.png">
        <title>Alumni - ESQBS</title>
        <!-- Custom CSS -->
        <link href="vendor/css/style.min.css" rel="stylesheet">
    </head>

    <body>
        <div class="preloader">
            <div class="lds-ripple">
                <div class="lds-pos"></div>
                <div class="lds-pos"></div>
            </div>
        </div>
        <div id="main-wrapper">
            <header class="topbar" data-navbarbg="skin5">
                <nav class="navbar top-navbar navbar-expand-md navbar-dark">
                    <div class="navbar-header" data-logobg="skin5">
                        <!-- This is for the sidebar toggle which is visible on mobile only -->
                        <a class="nav-toggler waves-effect waves-light d-block d-md-none" href="javascript:void(0)"><i class="ti-menu ti-close"></i></a>
                        <!-- ============================================================== -->
                        <!-- Logo -->
                        <!-- ============================================================== -->
                        <a class="navbar-brand" href="index.php">
                            <!-- Logo icon -->
                            <b class="logo-icon p-l-10">
                            <!--You can put here icon as well // <i class="wi wi-sunset"></i> //-->
                                <!-- Dark Logo icon -->
                                <!--img src="" alt="homepage" class="light-logo" /-->
                            </b>
                            <!--End Logo icon -->
                            <!-- Logo text --><span class="logo-text"><!-- dark Logo text -->
                            </span>
                        </a>
                        <a class="topbartoggler d-block d-md-none waves-effect waves-light" href="javascript:void(0)" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"><i class="ti-more"></i></a>
                    </div>
                    <div class="navbar-collapse collapse" id="navbarSupportedContent" data-navbarbg="skin5">
                        <ul class="navbar-nav float-left mr-auto">
                            <li class="nav-item search-box"> <a class="nav-link waves-effect waves-dark" href="javascript:void(0)"><i class="ti-search"></i></a>
                                <form class="app-search position-absolute">
                                    <input type="text" class="form-control" placeholder="Search &amp; enter"> <a class="srh-btn"><i class="ti-close"></i></a>
                                </form>
                            </li>
                        </ul>
                        <ul class="navbar-nav float-right">
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle waves-effect waves-dark" href="" id="2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="font-24 mdi mdi-bell font-24"></i>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right mailbox animated bounceInDown" aria-labelledby="2">
                                    <ul class="list-style-none">
                                        <li>
                                            <div class="">
                                                <!-- Message -->
                                                <a href="javascript:void(0)" class="link border-top">
                                                    <div class="d-flex no-block align-items-center p-10">
                                                        <span class="btn btn-success btn-circle"><i class="ti-calendar"></i></span>
                                                        <div class="m-l-10">
                                                            <h5 class="m-b-0">Acara</h5>
                                                            <span class="mail-desc">ngingetin tentang acara</span>
                                                        </div>
                                                    </div>
                                                </a>
                                                <!-- Message -->
                                                <a href="javascript:void(0)" class="link border-top">
                                                    <div class="d-flex no-block align-items-center p-10">
                                                        <span class="btn btn-info btn-circle"><i class="ti-settings"></i></span>
                                                        <div class="m-l-10">
                                                            <h5 class="m-b-0">Pengaturan</h5>
                                                            <span class="mail-desc">biasalah ngatur</span>
                                                        </div>
                                                    </div>
                                                </a>
                                                <!-- Message -->
                                                <a href="javascript:void(0)" class="link border-top">
                                                    <div class="d-flex no-block align-items-center p-10">
                                                        <span class="btn btn-primary btn-circle"><i class="ti-user"></i></span>
                                                        <div class="m-l-10">
                                                            <h5 class="m-b-0">Admin Ganteng</h5>
                                                            <span class="mail-desc">kepoin admin</span>
                                                        </div>
                                                    </div>
                                                </a>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            <li class="nav-item dropdown">
                                <?php
                                $username = $_SESSION['username'];
                                $p = mysqli_fetch_array(mysqli_query($con, "SELECT * FROM user WHERE id_user='$username'"));
                                ?>
                                <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark pro-pic" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="<?php echo 'img/photo_profile/' . $p['photo_profile']; ?>" alt="user" class="rounded-circle" width="31"></a>
                                <div class="dropdown-menu dropdown-menu-right user-dd animated">
                                    <a class="dropdown-item" href="profile.php?username=<?php echo $username ?>"><i class="ti-user m-r-5 m-l-5"></i> My Profile</a>
                                    <a class="dropdown-item" href="change_password.php?username=<?php echo $username ?>"><i class="ti-settings m-r-5 m-l-5"></i> Change Password</a>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="authentication-login.php"><i class="fa fa-power-off m-r-5 m-l-5"></i> Logout</a>
                                </div>
                            </li>
                        </ul>
                    </div>
                </nav>
            </header>
            <aside class="left-sidebar" data-sidebarbg="skin5">
                <!-- Sidebar scroll-->
                <div class="scroll-sidezbar">
                    <!-- Sidebar navigation-->
                    <nav class="sidebar-nav">
                        <ul id="sidebarnav" class="p-t-30">
                            <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="index.php" aria-expanded="false"><i class="mdi mdi-home"></i><span class="hide-menu">Home</span></a></li>
                            <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="data_alumni.php" aria-expanded="false"><i class="mdi mdi-account-card-details"></i><span class="hide-menu">Data Alumni</span></a></li>
                            <li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-glassdoor"></i><span class="hide-menu">Room</span></a>
                                <ul aria-expanded="false" class="collapse  first-level">
                                    <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="forum_forum.php" aria-expanded="false"><i class="fas fa-people-carry"></i><span class="hide-menu">Forum-forum</span></a></li>
                                    <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="blog.php" aria-expanded="false"><i class="fas fa-newspaper"></i><span class="hide-menu">Blog</span></a></li>
                                </ul>
                            </li>

                            <li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-animation"></i><span class="hide-menu">Media</span></a>
                                <ul aria-expanded="false" class="collapse  first-level">
                                    <li class="sidebar-item"><a href="pages-gallery.php" class="sidebar-link"><i class="fas fa-film"></i><span class="hide-menu"> Gallery </span></a></li>
                                    <li class="sidebar-item"><a href="pages-chat.php" class="sidebar-link"><i class="mdi mdi-message-outline"></i><span class="hide-menu"> Chat Option </span></a></li>
                                </ul>
                            </li>
                        </ul>
                    </nav>
                </div>
            </aside>

            <div class="page-wrapper">
                <div class="page-breadcrumb">
                    <div class="row">
                        <div class="col-12 d-flex no-block align-items-center">
                            <h4 class="page-title">Chat</h4>
                            <div class="ml-auto text-right">
                                <nav aria-label="breadcrumb">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                                        <li class="breadcrumb-item active" aria-current="page">Library</li>
                                    </ol>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="container-fluid">
                    <div class="row">
                        <?php
                        if (isset($_GET['friends'])) {
                            $friends = $_GET['friends'];
                            $chat = mysqli_query($con, "SELECT * FROM chat ORDER BY tanggal_waktu");
                            $temp_friends = mysqli_query($con, "SELECT * FROM user WHERE id_user='$friends'");

                            $data_friends = mysqli_fetch_array($temp_friends);
                            ?>
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-body">
                                        <h4 class="card-title"><i class="far fa-dot-circle online"></i> <?php echo $data_friends['nama']; ?></h4>
                                        <div class="chat-box scrollable" style="height:475px;">
                                            <!--chat Row -->
                                                <?php while ($chat_friends = mysqli_fetch_array($chat)) { ?>
                                                <ul class="chat-list">
                                                    <?php $dt = $chat_friends['tanggal_waktu']; ?>
                                                    <!--chat Friends -->
        <?php if (($chat_friends['dari'] == $friends) && ($chat_friends['ke'] == $username)) { ?>
                                                        <li class="chat-item">
                                                            <div class="chat-img"><img src="<?php echo 'img/photo_profile/' . $data_friends['photo_profile']; ?>" alt="user"></div>
                                                            <div class="chat-content">
                                                                <h6 class="font-medium"><?php echo $data_friends['nama']; ?></h6>
                                                                <div class="box bg-light-info"><?php echo $chat_friends['chat']; ?></div>
                                                            </div>
                                                            <div class="chat-time"><?php echo date("d/m/Y H:i:s", strtotime($dt)); ?></div>
                                                        </li>
        <?php } ?>
                                                </ul>
                                                <!--chat Self -->
                                                <ul class="chat-list">
        <?php if (($chat_friends['dari'] == $username) && ($chat_friends['ke'] == $friends)) { ?>
                                                        <li class="odd chat-item">
                                                            <div class="chat-content">
                                                                <div class="box bg-light-inverse"><?php echo $chat_friends['chat']; ?></div>
                                                                <br>
                                                            </div>
                                                            <div class="chat-time"><?php echo date("d/m/Y H:i:s", strtotime($dt)); ?></div>
                                                        </li>
                                                <?php } ?>
                                                </ul>
    <?php } ?>
                                        </div>
                                    </div>
                                    <form class="form-horizontal" method="post" action="config/save_chat.php?friends=<?php echo $friends ?>">
                                        <div class="card-body border-top">
                                            <div class="row">
                                                <div class="col-9">
                                                    <div class="input-field m-t-0 m-b-0">
                                                        <textarea id="textarea1" name="chat" placeholder="Type and enter" class="form-control border-0"></textarea>
                                                    </div>
                                                </div>
                                                <div class="col-3">
                                                    <button type="submit" class="btn-circle btn-lg btn-cyan float-right"><i class="fas fa-paper-plane"></i></button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
<?php } ?>
                    </div>
                </div>

                </br>
                </br>
                </br>
                <footer class="footer text-center">
                    Project Web Alumni ESQ Business School. Designed and Developed by <b>Computer Science I</b>
                </footer>
            </div>
            <div class="card-footer text-right fixed-bottom">
                <div class="btn-group dropup">
                    <button type="button" class="btn btn-lg btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fas fa-comment-alt"></i> Chat with Friends
                    </button>
                    <div class="dropdown-menu">
                        <?php
                        $all = mysqli_query($con, "SELECT * FROM user ORDER BY nama");
                        while ($admin = mysqli_fetch_array($all)) {
                            if (($username != $admin['id_user']) && $admin['level'] == "admin") {
                                ?>
                                <a class="dropdown-item" href="pages-chat.php?friends=<?php echo $admin['id_user']; ?>"><i class="fas fa-user"></i> <?php echo $admin['nama']; ?></a>
                            <?php }
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
        <script src="assets/libs/jquery/dist/jquery.min.js"></script>
        <!-- Bootstrap tether Core JavaScript -->
        <script src="assets/libs/popper.js/dist/umd/popper.min.js"></script>
        <script src="assets/libs/bootstrap/dist/js/bootstrap.min.js"></script>
        <!-- slimscrollbar scrollbar JavaScript -->
        <script src="assets/libs/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js"></script>
        <script src="assets/extra-libs/sparkline/sparkline.js"></script>
        <!--Wave Effects -->
        <script src="vendor/js/waves.js"></script>
        <!--Menu sidebar -->
        <script src="vendor/js/sidebarmenu.js"></script>
        <!--Custom JavaScript -->
        <script src="vendor/js/custom.min.js"></script>
    </body>

</html>
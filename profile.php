<?php
include "config/koneksi.php";
session_start();
if (!isset($_SESSION['username'])) {
    header("location:authentication-login.php");
}
$username = $_GET['username'];
?>
<!DOCTYPE html>
<html dir="ltr" lang="en">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Tell the browser to be responsive to screen width -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="stylesheet" href="vendor/css/style.css" />
        <!-- Favicon icon -->
        <link rel="icon" type="image/png" sizes="16x16" href="assets/images/favicon.png">
        <title>Alumni - ESQBS</title>
        <!-- Custom CSS -->
        <link rel="stylesheet" type="text/css" href="assets/libs/select2/dist/css/select2.min.css">
        <link rel="stylesheet" type="text/css" href="assets/libs/jquery-minicolors/jquery.minicolors.css">
        <link rel="stylesheet" type="text/css" href="assets/libs/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
        <link rel="stylesheet" type="text/css" href="assets/libs/quill/dist/quill.snow.css">
        <link href="vendor/css/style.min.css" rel="stylesheet">
        <![endif]-->
    </head>

    <body>
        <div class="preloader">
            <div class="lds-ripple">
                <div class="lds-pos"></div>
                <div class="lds-pos"></div>
            </div>
        </div>
        <div id="main-wrapper">
            <header class="topbar" data-navbarbg="skin5">
                <nav class="navbar top-navbar navbar-expand-md navbar-dark">
                    <div class="navbar-header" data-logobg="skin5">
                        <a class="nav-toggler waves-effect waves-light d-block d-md-none" href="javascript:void(0)"><i class="ti-menu ti-close"></i></a>
                        <a class="navbar-brand" href="index.php">
                            <!-- Logo icon -->
                            <b class="logo-icon p-l-10">
                            <!--You can put here icon as well // <i class="wi wi-sunset"></i> //-->
                                <!-- Dark Logo icon -->
                                <!--img src="img/logo/grad2.png" alt="homepage" class="light-logo" /-->
                            </b>
                            <!--End Logo icon -->
                            <!-- Logo text --><span class="logo-text"><!-- dark Logo text -->
                            </span>
                        </a>
                        <a class="topbartoggler d-block d-md-none waves-effect waves-light" href="javascript:void(0)" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"><i class="ti-more"></i></a>
                    </div>
                    <div class="navbar-collapse collapse" id="navbarSupportedContent" data-navbarbg="skin5">
                        <ul class="navbar-nav float-left mr-auto">
                            <li class="nav-item search-box"> <a class="nav-link waves-effect waves-dark" href="javascript:void(0)"><i class="ti-search"></i></a>
                                <form class="app-search position-absolute">
                                    <input type="text" class="form-control" placeholder="Search &amp; enter"> <a class="srh-btn"><i class="ti-close"></i></a>
                                </form>
                            </li>
                        </ul>
                        <ul class="navbar-nav float-right">
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle waves-effect waves-dark" href="" id="2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="font-24 mdi mdi-bell "></i>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right mailbox animated bounceInDown" aria-labelledby="2">
                                    <ul class="list-style-none">
                                        <li>
                                            <div class="">
                                                <!-- Message -->
                                                <a href="javascript:void(0)" class="link border-top">
                                                    <div class="d-flex no-block align-items-center p-10">
                                                        <span class="btn btn-success btn-circle"><i class="ti-calendar"></i></span>
                                                        <div class="m-l-10">
                                                            <h5 class="m-b-0">Acara</h5>
                                                            <span class="mail-desc">ngingetin tentang acara</span>
                                                        </div>
                                                    </div>
                                                </a>
                                                <!-- Message -->
                                                <a href="javascript:void(0)" class="link border-top">
                                                    <div class="d-flex no-block align-items-center p-10">
                                                        <span class="btn btn-info btn-circle"><i class="ti-settings"></i></span>
                                                        <div class="m-l-10">
                                                            <h5 class="m-b-0">Pengaturan</h5>
                                                            <span class="mail-desc">biasalah ngatur</span>
                                                        </div>
                                                    </div>
                                                </a>
                                                <!-- Message -->
                                                <a href="javascript:void(0)" class="link border-top">
                                                    <div class="d-flex no-block align-items-center p-10">
                                                        <span class="btn btn-primary btn-circle"><i class="ti-user"></i></span>
                                                        <div class="m-l-10">
                                                            <h5 class="m-b-0">Admin Ganteng</h5>
                                                            <span class="mail-desc">kepoin admin</span>
                                                        </div>
                                                    </div>
                                                </a>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            <li class="nav-item dropdown">
                                <?php $p = mysqli_fetch_array(mysqli_query($con, "SELECT * FROM user WHERE id_user='$username'")); ?>
                                <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark pro-pic" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="<?php echo 'img/photo_profile/' . $p['photo_profile']; ?>" alt="user" class="rounded-circle" width="31"></a>
                                <div class="dropdown-menu dropdown-menu-right user-dd animated">
                                    <a class="dropdown-item" href="profile.php?username=<?php echo $username ?>"><i class="ti-user m-r-5 m-l-5"></i> My Profile</a>
                                    <a class="dropdown-item" href="change_password.php?username=<?php echo $username ?>"><i class="ti-settings m-r-5 m-l-5"></i> Change Password</a>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="authentication-login.php"><i class="fa fa-power-off m-r-5 m-l-5"></i> Logout</a>
                                </div>
                            </li>
                        </ul>
                    </div>
                </nav>
            </header>
            <aside class="left-sidebar" data-sidebarbg="skin5">
                <!-- Sidebar scroll-->
                <div class="scroll-sidezbar">
                    <!-- Sidebar navigation-->
                    <nav class="sidebar-nav">
                        <ul id="sidebarnav" class="p-t-30">
                            <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="index.php" aria-expanded="false"><i class="mdi mdi-home"></i><span class="hide-menu">Home</span></a></li>
                            <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="data_alumni.php" aria-expanded="false"><i class="mdi mdi-account-card-details"></i><span class="hide-menu">Data Alumni</span></a></li>
                            <li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-glassdoor"></i><span class="hide-menu">Room</span></a>
                                <ul aria-expanded="false" class="collapse  first-level">
                                    <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="forum_forum.php" aria-expanded="false"><i class="fas fa-people-carry"></i><span class="hide-menu">Forum-forum</span></a></li>
                                    <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="blog.php" aria-expanded="false"><i class="fas fa-newspaper"></i><span class="hide-menu">Blog</span></a></li>
                                </ul>
                            </li>

                            <li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-animation"></i><span class="hide-menu">Media</span></a>
                                <ul aria-expanded="false" class="collapse  first-level">
                                    <li class="sidebar-item"><a href="pages-gallery.php" class="sidebar-link"><i class="fas fa-film"></i><span class="hide-menu"> Gallery </span></a></li>
                                    <li class="sidebar-item"><a href="pages-chat.php" class="sidebar-link"><i class="mdi mdi-message-outline"></i><span class="hide-menu"> Chat Option </span></a></li>
                                </ul>
                            </li>
                        </ul>
                    </nav>
                </div>
            </aside>
            <div class="page-wrapper">
                <div class="page-breadcrumb">
                    <div class="row">
                        <div class="col-12 d-flex no-block align-items-center">
                            <h4 class="page-title">Edit Profile</h4>
                            <div class="ml-auto text-right">
                                <nav aria-label="breadcrumb">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                                        <li class="breadcrumb-item active" aria-current="page">Profile</li>
                                    </ol>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="card">
                                <div class="card-img">
                                    <?php
                                    $profile = mysqli_query($con, "SELECT * FROM user WHERE id_user='$username'");
                                    $dataProfile = mysqli_fetch_array($profile);
                                    ?>
                                    <img src="<?php echo 'img/photo_profile/' . $dataProfile['photo_profile']; ?>" class="img-fluid">
                                </div>
                                <div class="card-body">
                                    <div class="text-center">
                                        <button type="button" class="btn btn-info" data-toggle="modal" data-target="#modalUpdateFoto">Update Foto</button>
                                    </div>
                                </div>

                                <div id="modalUpdateFoto" class="modal fade">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title">Ganti Foto Profile</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <form class="" method="post" action="config/edit_foto_profile.php?username=<?php echo $username ?>" enctype="multipart/form-data">
                                                <div class="modal-body">
                                                    <div class="form-group">
                                                        <label>Upload Foto</label>
                                                        <input type="file" name="photo_profile" id="photo_profile" />
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                                    <button type="submit" class="btn btn-info">Update Foto</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                    <!-- Modals -->
                                </div>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="card">
                                <form class="form-horizontal" method="post" action="config/edit_profile.php?username=<?php echo $dataProfile['id_user'] ?>">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label for="username">Username</label>
                                                    <input type="text" class="form-control" id="username" name="username" value="<?php echo $dataProfile['id_user']; ?>" readonly="readonly">
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label for="email">Email</label>
                                                    <input type="email" class="form-control" name="email" placeholder="email" id="email" value="<?php echo $dataProfile['email'] ?>">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <label for="namaDepan">Nama Lengkap</label>
                                                    <input type="text" class="form-control" name="nama" placeholder="nama lengkap" id="nama" value="<?php echo $dataProfile['nama']; ?>">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <label for="namaDepan">Tempat Lahir</label>
                                                    <input type="text" class="form-control" name="tempat_lahir" placeholder="tempat lahir" id="tempat_lahir" value="<?php echo $dataProfile['tempat_lahir']; ?>">
                                                </div>
                                            </div>
                                            <div class="col-sm-8">
                                                <label for="tanggalLahir">Tanggal Lahir</label>
                                                <div class="row ">
                                                    <div class="col-sm-4">
                                                        <div class="input-group mb-2">
                                                            <div class="input-group-prepend">
                                                                <div class="input-group-text">D</div>
                                                            </div>
                                                            <select name="tanggal" class="form-control custom-select">
                                                                <?php
                                                                $tanggal = 01;
                                                                $d_now = date('d', strtotime($dataProfile['tanggal_lahir']));
                                                                while ($tanggal < 32) {
                                                                    if ($d_now == $tanggal) {
                                                                        ?>
                                                                        <option selected value="<?php echo $tanggal; ?>"><?php echo $tanggal++; ?></option>
                                                                    <?php } else { ?>
                                                                        <option value="<?php echo $tanggal; ?>"><?php echo $tanggal++; ?></option>
                                                                    <?php
                                                                    }
                                                                }
                                                                ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <div class="input-group mb-2">
                                                            <div class="input-group-prepend">
                                                                <div class="input-group-text">M</div>
                                                            </div>
                                                            <select name="bulan" class="form-control custom-select">
                                                                <?php
                                                                $bulan = 01;
                                                                $m_now = date('m', strtotime($dataProfile['tanggal_lahir']));
                                                                while ($bulan < 13) {
                                                                    if ($m_now == $bulan) {
                                                                        ?>
                                                                        <option selected value="<?php echo $bulan; ?>"><?php echo $bulan++; ?></option>
                                                                    <?php } else { ?>
                                                                        <option value="<?php echo $bulan; ?>"><?php echo $bulan++; ?></option>
                                                                    <?php
                                                                    }
                                                                }
                                                                ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <div class="input-group mb-2">
                                                            <div class="input-group-prepend">
                                                                <div class="input-group-text">Y</div>
                                                            </div>
                                                            <select name="tahun" class="form-control custom-select">
                                                                <?php
                                                                $tahun = 1990;
                                                                $y_now = date('Y', strtotime($dataProfile['tanggal_lahir']));
                                                                while ($tahun < 2011) {
                                                                    if ($y_now == $tahun) {
                                                                        ?>
                                                                        <option selected value="<?php echo $tahun; ?>"><?php echo $tahun++; ?></option>
                                                                    <?php } else { ?>
                                                                        <option value="<?php echo $tahun; ?>"><?php echo $tahun++; ?></option>
                                                                    <?php
                                                                    }
                                                                }
                                                                ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label for="username">Nomor Telepon</label>
                                                    <input type="text" name="nomor_kontak" class="form-control" data-mask="0000-0000-0000" data-mask-clearifnotmatch="true" placeholder="0000-0000-0000" value="<?php echo $dataProfile['nomor_kontak']; ?>" />
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label for="email">Status Perkawinan</label>
                                                    <select name="status_perkawinan" class="form-control custom-select">
                                                        <?php $sp = array('Belum Menikah', 'Menikah', 'Janda/Duda'); ?>
                                                        <?php for ($x = 0; $x < count($sp); $x++) {
                                                            if ($dataProfile['status_perkawinan'] == $sp[$x]) {
                                                                ?>
                                                                <option selected value="<?php echo $sp[$x] ?>"><?php echo $sp[$x] ?></option>
    <?php } else { ?>
                                                                <option value="<?php echo $sp[$x] ?>"><?php echo $sp[$x] ?></option>
    <?php }
}
?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <label for="alamat">Alamat</label>
                                                    <textarea class="form-control" type="textarea" id="alamat" name="alamat" placeholder="Alamat" maxlength="140" rows="3"><?php echo $dataProfile['alamat']; ?></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="border-top">
                                        <div class="card-body text-right">
                                            <button type="reset" class="btn btn-warning">Reset</button>
                                            <button type="submit" class="btn btn-info">Update Profile</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                </br>
                </br>
                </br>
                <footer class="footer text-center">
                    Project Web Alumni ESQ Business School. Designed and Developed by <b>Computer Science I</b>
                </footer>
            </div>
            <div class="card-footer text-right fixed-bottom">
                <div class="btn-group dropup">
                    <button type="button" class="btn btn-lg btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fas fa-comment-alt"></i> Chat with Friends
                    </button>
                    <div class="dropdown-menu">
                        <?php
                        $all = mysqli_query($con, "SELECT * FROM user ORDER BY nama");
                        while ($admin = mysqli_fetch_array($all)) {
                            if (($username != $admin['id_user']) && $admin['level'] == "admin") {
                                ?>
                                <a class="dropdown-item" href="pages-chat.php?friends=<?php echo $admin['id_user']; ?>"><i class="fas fa-user"></i> <?php echo $admin['nama']; ?></a>
    <?php }
}
?>
                    </div>
                </div>
            </div>
        </div>

        <script src="assets/libs/jquery/dist/jquery.min.js"></script>
        <!-- Bootstrap tether Core JavaScript -->
        <script src="assets/libs/popper.js/dist/umd/popper.min.js"></script>
        <script src="assets/libs/bootstrap/dist/js/bootstrap.min.js"></script>
        <!-- slimscrollbar scrollbar JavaScript -->
        <script src="assets/libs/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js"></script>
        <script src="assets/extra-libs/sparkline/sparkline.js"></script>
        <!--Wave Effects -->
        <script src="vendor/js/waves.js"></script>
        <!--Menu sidebar -->
        <script src="vendor/js/sidebarmenu.js"></script>
        <!--Custom JavaScript -->
        <script src="vendor/js/custom.min.js"></script>
        <!-- This Page JS -->
        <script src="assets/libs/inputmask/dist/min/jquery.inputmask.bundle.min.js"></script>
        <script src="vendor/js/pages/mask/mask.init.js"></script>
        <script src="assets/libs/select2/dist/js/select2.full.min.js"></script>
        <script src="assets/libs/select2/dist/js/select2.min.js"></script>
        <script src="assets/libs/jquery-asColor/dist/jquery-asColor.min.js"></script>
        <script src="assets/libs/jquery-asGradient/dist/jquery-asGradient.js"></script>
        <script src="assets/libs/jquery-asColorPicker/dist/jquery-asColorPicker.min.js"></script>
        <script src="assets/libs/jquery-minicolors/jquery.minicolors.min.js"></script>
        <script src="assets/libs/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
        <script src="assets/libs/quill/dist/quill.min.js"></script>
        <script>
            //***********************************//
            // For select 2
            //***********************************//
            $(".select2").select2();
            /*colorpicker*/
            $('.demo').each(function () {
                //
                // Dear reader, it's actually very easy to initialize MiniColors. For example:
                //
                //  $(selector).minicolors();
                //
                // The way I've done it below is just for the demo, so don't get confused
                // by it. Also, data- attributes aren't supported at this time...they're
                // only used for this demo.
                //
                $(this).minicolors({
                    control: $(this).attr('data-control') || 'hue',
                    position: $(this).attr('data-position') || 'bottom left',
                    change: function (value, opacity) {
                        if (!value)
                            return;
                        if (opacity)
                            value += ', ' + opacity;
                        if (typeof console === 'object') {
                            console.log(value);
                        }
                    },
                    theme: 'bootstrap'
                });
            });
            /*datwpicker*/
            jQuery('.mydatepicker').datepicker();
            jQuery('#datepicker-autoclose').datepicker({
                autoclose: true,
                todayHighlight: true
            });
            var quill = new Quill('#editor', {
                theme: 'snow'
            });
        </script>
    </body>  
</html>